'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BASE_API: '"http://api.xxx.com/"',
  RESOURCE_DOMAIN:'"http://api.xxx.com"',
  WS_DOMAIN:'"api.xxx.com"',
  PROJECT_NAME:'"SKY_SERVER"',
}
