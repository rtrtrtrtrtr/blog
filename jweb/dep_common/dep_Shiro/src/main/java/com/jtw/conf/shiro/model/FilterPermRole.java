package com.jtw.conf.shiro.model;

import lombok.Data;

import java.util.List;

/**
 * DESCRIPT: 角色辅助类
 *
 * @author cjsky666
 * @date 2018/9/10 15:56
 */
@Data
public class FilterPermRole {
    private String HttpMethod;
    private List<BaseRole> baseRoleList;
}
