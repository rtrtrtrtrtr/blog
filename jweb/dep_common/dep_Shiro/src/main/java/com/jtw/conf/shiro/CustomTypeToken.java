package com.jtw.conf.shiro;

import com.jtw.conf.shiro.enums.LoginType;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * DESCRIPT: 自定义登陆认证器
 *
 * @author cjsky666
 * @date 2019/7/24 22:03
 */
public class CustomTypeToken extends UsernamePasswordToken {
    private LoginType type;
    public CustomTypeToken(){
        super();
    }
    public CustomTypeToken(String username, String password, LoginType type, boolean rememberMe,  String host) {
        super(username, password, rememberMe,  host);
        this.type = type;
    }
    /**免密登录*/
    public CustomTypeToken(String username) {
        super(username, "", false, null);
        this.type = LoginType.NOPASSWD;
    }
    /**账号密码登录*/
    public CustomTypeToken(String username, String password) {
        super(username, password, false, null);
        this.type = LoginType.PASSWORD;
    }
    public LoginType getType() {
        return type;
    }
    public void setType(LoginType type) {
        this.type = type;
    }
}
