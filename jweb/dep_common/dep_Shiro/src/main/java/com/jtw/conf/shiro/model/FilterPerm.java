package com.jtw.conf.shiro.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * DESCRIPT:权限辅助类
 *
 * @author cjsky666
 * @date 2018/9/10 15:55
 */

@Data
@Slf4j
public class FilterPerm {
    private String url;
    private List<FilterPermRole> filterPermRoleList;
    private Boolean open;
    private Integer state;

    /**
     * 生成filter字串
     * @param filterKey filter关键字
     * @return
     */
    public String getFilterString(String filterKey) {
        StringBuffer filterString = new StringBuffer(filterKey + "[");
        filterString.append(getFilterString());
        filterString.append("]");
        return filterString.toString();
    }

    public String getFilterString() {
        StringBuffer filterString = new StringBuffer();
        int i = 0;
        for (FilterPermRole filterPermRole : filterPermRoleList) {
            log.info("------当前需要权限验证的配置数据是-----"+filterPermRole);
            if (i++ > 0) filterString.append(",");
            filterString.append(filterPermRole.getHttpMethod().toUpperCase()).append(":");
            int j = 0;
            for (BaseRole role : filterPermRole.getBaseRoleList()) {
                if (j++ > 0) filterString.append("|");
                filterString.append(role.getId());
            }
        }
        log.info("此条需要认证的权限信息是"+filterString.toString());
        return filterString.toString();
    }
}
