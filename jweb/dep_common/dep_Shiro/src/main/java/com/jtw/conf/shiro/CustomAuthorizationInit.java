//package com.jtw.conf.shiro;
//
//import com.jtw.conf.shiro.service.BasePermService;
//import com.jtw.conf.shiro.util.ShiroUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.event.ContextRefreshedEvent;
//
///**
// * DESCRIPT: 系统启动时根据数据库，初始化shiro全局权限
// *    若处理了系统超级管理员权限的动态生成 则此方法可以注释掉。
// * @author cjsky666
// * @date 2018/9/5 15:27
// */
//@Slf4j
//@Configuration
//@EnableConfigurationProperties
//public class CustomAuthorizationInit implements ApplicationListener<ContextRefreshedEvent> {
//    @Autowired
//    BasePermService basePermService;
//    @Autowired
//    ShiroFilterFactoryBean shiroFilterFactoryBean;
//    @Override
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//        try {
//            log.info("更新权限");
//            ShiroUtil.updateFilterChain(shiroFilterFactoryBean,basePermService);
//        } catch (Exception e) {
//            log.error("权限配置失败"+e);
//        }
//    }
//}
