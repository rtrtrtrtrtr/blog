package com.jtw.sys.controller.asset;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.service.member.MemberAssetServiceImpl;
import com.jtw.sys.vo.asset.SysMemberAssetUpdateReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/2 11:20
 */
@Api(tags = "资金管理模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/asset")
public class SysAssetController {

    @Autowired
    private MemberAssetServiceImpl memberAssetServiceImpl;


    @ApiOperation(notes = "系统会员资金组", value = "查询系统积分记录")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "userName",prefix = "`t_sys_user`."),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "serialNo"),
                    @FindPagingItemAnno(conditions = {"="},column = "type", fixedValue = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","999"}, isString = false),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "createTime", prefix = "`t_sys_member_asset_log`."),
            })
    @GetMapping("/findAllMemberAssetChangeLog")
    @PermAnno(sort = 7, group = 11)
    public Message findAllMemberAssetChangeLog(@Valid FindPagingVo findPagingVo){
        return ReqCode.SelectSuccess.getMessage(memberAssetServiceImpl.findAllMemberAssetChangeLog(findPagingVo));
    }

    @ApiOperation(notes = "系统会员资金组", value = "查询会员积分信息")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "userName"),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "balance", isString = false),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "freezeBalance", isString = false),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "createTime", prefix = "`t_sys_member_asset`."),
            })
    @GetMapping("/findAllMemberAssetDetail")
    @PermAnno(sort = 8, group = 11)
    public Message findAllMemberAssetDetail(@Valid FindPagingVo findPagingVo){
        return ReqCode.SelectSuccess.getMessage(memberAssetServiceImpl.findAllMemberAssetDetail(findPagingVo));
    }

    @ApiOperation(notes = "系统会员资金组", value = "会员积分调整")
    @PostMapping("/adjustMemberAsset")
    @PermAnno(sort = 9, group = 11)
    public Message adjustMemberAsset(HttpServletRequest request,
                                     @NotNull(message = "id不能为空") Integer id,
                                     @NotNull(message = "积分不能为空") @Min(value = 0,message = "最小值为0") Integer balance,
                                     @NotNull(message = "冻结积分不能为空") @Min(value = 0,message = "最小值为0") Integer freezeBalance
                                     ){
        SysMemberAssetUpdateReq sysMemberAssetUpdateReq = new SysMemberAssetUpdateReq();
        sysMemberAssetUpdateReq.setId(id);
        sysMemberAssetUpdateReq.setBalance(balance);
        sysMemberAssetUpdateReq.setFreezeBalance(freezeBalance);
        memberAssetServiceImpl.adjustMemberAsset(request,sysMemberAssetUpdateReq);
        return ReqCode.SubmitSuccess.getMessage();
    }
}
