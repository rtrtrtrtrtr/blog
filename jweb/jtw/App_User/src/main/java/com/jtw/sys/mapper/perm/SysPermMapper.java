package com.jtw.sys.mapper.perm;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.perm.TSysPerm;
import com.jtw.sys.vo.perm.SysPermTreeVo;
import com.jtw.sys.vo.perm.SysRolePermAddVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SysPermMapper extends MyMapper<TSysPerm> {
    @Select("SELECT" +
            " `t_sys_perm_group`.`DESC` ," +
            " `t_sys_perm`.*" +
            " FROM `t_sys_perm_group`" +
            " LEFT JOIN `t_sys_perm`" +
            " ON `t_sys_perm_group`.`GROUP` = `t_sys_perm`.`GROUP`")
    Page<TSysPerm> findAll();

    @Select("SELECT `URL` FROM `t_sys_perm` ORDER BY SORT;")
    List<String> getAllPermUrl();

    @Delete("DELETE FROM `ct_sys_role_perm` WHERE ROLE_ID = #{roleId};")
    void deleteSysRolePermByRoleId(Integer roleId);

    void modifyRolePerms(@Param("roleId")Integer roleId,@Param("perms") Integer[] perms);

    @Select("SELECT `ID`,`DESC`" +
            " FROM `t_sys_perm` " +
            " WHERE" +
            " `OPEN` = FALSE AND `STATE`= 1 AND ID NOT IN (" +
            "SELECT `PERM_ID` FROM `ct_sys_role_perm` WHERE ROLE_ID = #{roleId}" +
            ") ORDER BY `SORT`;")
    List<SysRolePermAddVo> getAllRolePermsByRoleIdNotHave(Integer roleId);

    @Select("SELECT `ID`,`DESC` FROM `t_sys_perm` WHERE `OPEN` = FALSE AND `STATE`= 1 AND ID IN (SELECT `PERM_ID` FROM `ct_sys_role_perm` WHERE ROLE_ID = #{roleId}) ORDER BY `SORT`;")
    List<SysRolePermAddVo> getAllRolePermsByRoleId(Integer roleId);

    @Select("SELECT COUNT(1) FROM `t_sys_perm` WHERE ID = #{id};")
    boolean isSysPermExisting(Integer id);
    @Update("UPDATE `t_sys_perm` " +
            "SET " +
            " `TYPE` = #{type}," +
            " `DESC` = #{desc}," +
            " `GROUP` = #{group}," +
            " `SORT` = #{sort}," +
            " `STATE` = #{state}," +
            " `OPEN` = #{open}" +
            " WHERE `ID` = #{id};")
    int updateSysPerm(TSysPerm sysPerm);

    List<SysPermTreeVo> getAllSysPermTreeMarkByRoleId(@Param("roleId") Integer roleId);
}