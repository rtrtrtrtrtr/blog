package com.jtw.sys.model.draw;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:抽奖活动奖品表
 *
 * @author cjsky666
 * @date 2019/7/18 13:54
 */
@Data
@Table(name = "`t_sys_draw_prize`")
public class TSysDrawPrize implements Serializable {
    private static final long serialVersionUID = 705119885174322716L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer prizeTypeId;
    private Integer drawId;
    private String prizeName;
    private String prizeNickName;
    private String image;
    private String ThumbNailImage;
    private Integer total;
    private String descript;
    private Integer weight;
    private String operatorIp;
    private Integer prizeType;
    private Integer price;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
