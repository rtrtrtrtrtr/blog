package com.jtw.sys.constants;

import org.joda.time.LocalDateTime;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * DESCRIPT:系统常量包
 *
 * @author cjsky666
 * @date 2018/9/14 14:39
 */
public class Constant {

    public static final String KEY_SYSTEM_VERSION="platformVersion";
    public static final String KEY_SYSTEM_VERSION_VALUE="V1_20190801";

    public static final String KEY_SPRING_SESSION_PREFFIX = "spring:session:sessions:";//redis中的sessionKEY前缀
    public static final String KEY_SPRING_SESSION_EXPIRES_PREFFIX = "spring:session:sessions:expires:";//redis中的session过期时间的前缀KEY前缀
    public static final String KEY_USER_INFO="user";//用户信息常量
    public static final String KEY_USER_USER_NAME="userName";//用户的username
    public static final String KEY_CLIENT_TOKEN="token";//用户信息常量
    public static final String USER_PRIMARY_KEY="id";//用户主键id常量
    public static final String KEY_CSRF_TOKEN="CSRFToken";//跨站请求伪造检测token常量
    public static final String KEY_CAPTCHA="rand";//验证码常量
    public static final String KEY_SHARE_TOKEN="shareToken";//分享令牌
    /**
     * 系统中2个不能删除的基本角色ID，1、超级管理员角色2、会员角色，
     */
    public static final Integer ROLE_ID_SUPER_ADMIN=1;
    public static final Integer ROLE_ID_MEMBER=2;
    /**
     * 超级管理员的账号id为1，不允许删除
     */
    public static final Long USER_ID_SUPER_ADMIN=1L;
    /**
     * 性别常量1、男2、女0、保密
     */
    public static final Integer SEX_MAN = 1;
    public static final Integer SEX_WOMAN = 2;
    public static final Integer SEX_UNKNOW = 0;
    /**
     * 系统配置项中的通道是否锁定标志 0为正常，1为锁定
     */
    public static final String CHANNEL_LOCKED = "0";//

    public static final Integer DRAW_GAME_CODE = 0;//赛事竞猜 平局常量
    public static final String  DRAW_GAME_CODE_DESC =  "平局";  //赛事竞猜平局常量描述

    //系统初始化配置
    public static final String KEY_WEB_NAME="webName";//网站名称（显示于客户端,后台登陆、管理界面）
    public static final String KEY_WEB_LOGO="webLogo";//网站logo图标
    public static final String KEY_SERVICE_LOCKED="serviceLocked";//网站访问锁定(1代表锁定，其他不锁定)
    public static final String KEY_SERVICE_LOCKED_INFO="serviceLockedInfo";//网站访问锁定关闭提示
    public static final String KEY_CUSTOM_SERVICE_LINK="customServiceLink";//客服链接
    public static final String KEY_FRIENDLY_REMINDER="friendlyReminder";//温馨提示
    public static final String KEY_COMPANY_NAME="companyName";//公司标志
    public static final String KEY_CLIENT_BG_IMAGE="clientBgImage";//网站移动客户端背景图
    public static final String KEY_CLIENT_BG_IMAGE_PC="clientBgImagePC";//网站PC客户端端背景图
    public static final String KEY_CHECK_IN_REWARD = "checkInReward";//签到奖励数值
    public static final String KEY_CHECK_IN_COUNT = "checkInCount";//可签到次数
    public static final String KEY_WXGZH_SHARE_REWARD = "wxgzhShareReward";//分享的奖励额度

    public static final String KEY_NOTICE_BAR_ID = "noticeBarId";//通告栏资讯id
    public static Map<String,String> sysConfMap = new HashMap<String,String>();//系统配置键值对

    //公众号 appId和appSecret
    public static final String WXGZH_APP_ID = "XXXXXXXXXXXXXXXXX";
    public static final String WXGZH_APP_SECRET = "XXXXXXXXXXXXXXXXXXXXXXX";

    public static LocalDateTime wxgzhShareTicketTime = LocalDateTime.now();
    public static LocalDateTime wxgzhAccessTokenTime = LocalDateTime.now();
    public static String WXGZH_SHARE_JS_API_TICKET = null;
    public static String WXGZH_ACCESS_TOKEN = null;
    public static String WXGZH_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    public static String WXGZH_SHARE_JS_API_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=ACCESS_TOKEN";

    //公众号网页授权登陆获取access_code地址
    public static String WXGZH_H5_CLIENT_AUTH_ACCESS_TOKEN_URL="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
    //公众号网页授权之后拉取用户基本信息
    public static String WXGZH_H5_CLIENT_AUTH_USER_INFO_URL="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
    //公众号网页授权之后 检验授权凭证（access_token）是否有效
    public static String WXGZH_H5_CLIENT_AUTH_VALIDATED_ACCESSION_URL="https://api.weixin.qq.com/sns/auth?access_token=ACCESS_TOKEN&openid=OPENID";


}
