//package com.jtw.mqconf;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//
//@Component
//@RabbitListener(queues = "hello")
//public class HelloRecevier {
//    @Autowired
//    private RedisTemplate redisTemplate;
//
//    @RabbitHandler
//    public void process(String hello){
//        System.out.println("接收到了"+hello);
//    }
//}
