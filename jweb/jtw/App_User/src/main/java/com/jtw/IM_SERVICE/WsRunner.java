package com.jtw.IM_SERVICE;

import com.jtw.ws.TioWsConfig;
import com.jtw.ws.WsHandler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 开启类
 */
@Component
public class WsRunner implements CommandLineRunner {

    @Resource
    private WsHandler myWsHandler;

    private TioWsStarter appStarter;

    @Override
    public void run(String... args) throws Exception {
        this.appStarter = new TioWsStarter(TioWsConfig.SERVER_PORT, myWsHandler);
        appStarter.getWsServerStarter().start();
    }

}
