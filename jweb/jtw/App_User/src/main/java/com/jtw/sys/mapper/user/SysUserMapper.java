package com.jtw.sys.mapper.user;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.model.user.TSysUser;
import com.jtw.sys.vo.role.SysRoleUpdateVo;
import com.jtw.sys.vo.user.SysUserAccountPermVo;
import com.jtw.sys.vo.user.SysUserInfoBaseVo;
import com.jtw.sys.vo.user.UserWithdrawPwdResetVo;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

/**
 * DESCRIPT:系统用户角色DAO
 *
 * @author cjsky666
 * @date 2018/9/14 15:35
 */
public interface SysUserMapper extends MyMapper<TSysUser> {
    Page<TSysUser> findAll(PagingVo pagingVo);

    @Select("SELECT " +
            " t_sys_user.`ID`," +
            " `USER_NAME`," +
            " `STATE`," +
            " `NICK_NAME`," +
            " `THUMB_NAIL_IMAGE`," +
            " `HEAD_IMAGE`," +
            " `ADDRESS`,"+
            " `SEX`," +
            " `AGE`," +
            " `MOBILE`," +
            " `QQ`," +
            " `DESC`," +
            " `EMAIL`," +
            " `LAST_LOGIN_TIME`," +
            " `t_sys_user_info`.CREATE_TIME," +
            " `CURRENT_LOGIN_TIME`" +
            " FROM `t_sys_user`"+
            " LEFT JOIN `t_sys_user_info`" +
            " ON t_sys_user.ID = t_sys_user_info.ID"+
            " WHERE `t_sys_user`.ID = (SELECT ID FROM `t_sys_user` WHERE USER_NAME = #{userName});")
    SysUserInfoBaseVo getUserInfoBaseVoByUserName(@Param("userName") String userName);

    @Select("SELECT COUNT(0) FROM `ct_sys_user_admin` WHERE `USER_ID` = #{id}")
    Boolean isAdmin(@Param("id") Long id);

    @Update("UPDATE `t_sys_user_info` SET" +
            " `LAST_LOGIN_TIME` = `t_sys_user_info`.CURRENT_LOGIN_TIME," +
            " `CURRENT_LOGIN_TIME` = #{date}" +
            " WHERE ID = #{id};")
    int updateUserLoginTime(@Param("id") Long id, @Param("date") Date date);

    int insertUserRolesByUserId(@Param("id") Long id, @Param("roleList")Integer [] roleList,@Param("operator") String operator,@Param("operatorIp")String operatorIp);

    @Delete("DELETE FROM `ct_sys_user_role` WHERE USER_ID = #{userId};")
    int delUserRolesByUserId(@Param("userId")Long userId);

    @Select("SELECT" +
            " `ID`," +
            " `NAME`," +
            " `DESC`" +
            " FROM" +
            " `t_sys_role`" +
            "WHERE" +
            " `ID` IN (SELECT ROLE_ID FROM `ct_sys_user_role` WHERE USER_ID = #{userId});")
    List<SysRoleUpdateVo> getUserRolesByUserId(@Param("userId")Long userId);

    @Select("SELECT" +
            " `ID`," +
            " `NAME`," +
            " `DESC`" +
            " FROM" +
            " `t_sys_role`" +
            "WHERE" +
            " `ID` NOT IN (SELECT ROLE_ID FROM `ct_sys_user_role` WHERE USER_ID = #{userId});")
    List<SysRoleUpdateVo> getAllRolesByUserIdNotHave(@Param("userId")Long userId);

    List<SysUserAccountPermVo> getAllPermsBySysUserId(@Param("userId")Long userId);

    @Select("SELECT " +
            " t_sys_user.`ID`," +
            " `USER_NAME`," +
            " `STATE`," +
            " `NICK_NAME`," +
            " `THUMB_NAIL_IMAGE`," +
            " `HEAD_IMAGE`," +
            " `ADDRESS`,"+
            " `SEX`," +
            " `AGE`," +
            " `MOBILE`," +
            " `QQ`," +
            " `DESC`," +
            " `EMAIL`," +
            " `LAST_LOGIN_TIME`," +
            " `CURRENT_LOGIN_TIME`," +
            " `t_sys_user_info`.CREATE_TIME" +
            " FROM `t_sys_user`"+
            " LEFT JOIN `t_sys_user_info`" +
            " ON `t_sys_user`.ID = `t_sys_user_info`.ID" +
            " ${whereSQL}")
    Page<SysUserInfoBaseVo> findAllUserInfoBaseVo(@Param("whereSQL") String whereSQL);

    @Select("SELECT " +
            " t_sys_user.`ID`," +
            " `USER_NAME`," +
            " `STATE`," +
            " `NICK_NAME`," +
            " `THUMB_NAIL_IMAGE`," +
            " `HEAD_IMAGE`," +
            " `ADDRESS`,"+
            " `SEX`," +
            " `AGE`," +
            " `MOBILE`," +
            " `QQ`," +
            " `DESC`," +
            " `EMAIL`," +
            " `LAST_LOGIN_TIME`," +
            " `CURRENT_LOGIN_TIME`," +
            " `t_sys_user_info`.CREATE_TIME" +
            " FROM `t_sys_user`"+
            " LEFT JOIN `t_sys_user_info`" +
            " ON t_sys_user.ID = t_sys_user_info.ID"+
            " WHERE `t_sys_user`.ID = #{userId};")
    SysUserInfoBaseVo getUserInfoBaseVoByUserId(Long userId);

    @Select("SELECT count(0) FROM `t_sys_user` WHERE `USER_NAME` = #{userName}")
    boolean isExistsWithUserName(@Param("userName") String userName);

    @Insert("INSERT INTO `ct_sys_user_admin` VALUES (#{userId});")
    void addAdminLoginPerm(@Param("userId") long userId);

    @Insert("DELETE FROM `ct_sys_user_admin` WHERE `USER_ID` = #{userId};")
    void removeAdminLoginPerm(@Param("userId")long userId);

    @Select("SELECT * FROM `t_sys_member_withdraw_pwd_reset` ${whereSQL}")
    Page<UserWithdrawPwdResetVo> findAllWithdrawPwdResetApply(@Param("whereSQL") String whereSQL);

    @Select("SELECT * FROM `t_sys_member_pwd_reset` ${whereSQL}")
    Page<UserWithdrawPwdResetVo> findAllPwdResetApply(@Param("whereSQL") String whereSQL);

    @Insert("INSERT INTO `ct_sys_member_invite_account` (`USER_ID`,`PARENT`) VALUES (#{userId},#{parent})")
    void saveMemberToInviteAccount(@Param("userId")Long userId,@Param("parent") Long parent);

    @Select("SELECT `USER_ID` FROM `ct_sys_member_team` WHERE `PARENT` = #{parent}")
    List<Long> getAllTeamMemberByParent(Long parent);
}