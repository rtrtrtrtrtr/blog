package com.jtw.sys.vo.perm;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:修改系统角色权限请求模型
 *
 * @author cjsky666
 * @date 2018/9/18 15:53
 */
@Data
public class SysRolePermAddReq implements Serializable {
    private static final long serialVersionUID = -2847394459020810668L;
    @NotNull(message = "角色id不能为空")
    private Integer roleId;
    @NotEmpty(message = "请求修改的权限不能为空")
    private Integer[] perms;
}
