package com.jtw.sys.model.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 签到记录表
 *
 * @author cjsky666
 * @date 2019/7/8 10:03
 */
@Data
@Table(name = "`t_sys_member_check_in`")
public class TSysMemberCheckIn implements Serializable {
    private static final long serialVersionUID = 6519216772907405197L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Long userId;
    private String userName;
    @JsonFormat(pattern="yyyy/MM/dd",timezone="GMT+8")
    private Date checkInDate;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;

}
