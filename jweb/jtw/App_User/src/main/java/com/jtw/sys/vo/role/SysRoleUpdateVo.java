package com.jtw.sys.vo.role;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/20 16:11
 */
@Data
public class SysRoleUpdateVo implements Serializable {
    private static final long serialVersionUID = 825029264041451750L;
    private Integer id;
    private String name;
    private String desc;
}
