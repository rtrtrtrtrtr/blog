package com.jtw.sys.service.task;

import com.MyMapper;
import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.task.TSysTask;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 10:07
 */
public interface SysTaskService  extends BaseService<TSysTask> {
}
