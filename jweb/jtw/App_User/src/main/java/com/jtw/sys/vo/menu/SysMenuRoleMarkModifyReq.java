package com.jtw.sys.vo.menu;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/22 14:11
 */
@Data
public class SysMenuRoleMarkModifyReq implements Serializable {
    private static final long serialVersionUID = -6608063428832476864L;
    @NotNull(message = "角色id不能为空")
    private Integer roleId;
    @NotEmpty(message = "保存的菜单项至少有一个")
    private Integer menus[];
}
