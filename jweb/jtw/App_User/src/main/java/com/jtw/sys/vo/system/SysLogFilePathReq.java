package com.jtw.sys.vo.system;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/5/15 16:29
 */
@Data
public class SysLogFilePathReq implements Serializable {
    private static final long serialVersionUID = -6668589751676608967L;
    @NotEmpty(message = "文件为空")
    private String [] paths;
}
