package com.jtw.sys.model.log;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/21 12:59
 */
@Data
@Table(name = "`t_sys_user_login_log`")
public class TSysUserLoginLog implements Serializable {
    private static final long serialVersionUID = 800127252043201823L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Long id;
    private Long userId;
    private String ip;
    private String address;
    private String device;
    private Integer loginMode;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date loginTime;
}
