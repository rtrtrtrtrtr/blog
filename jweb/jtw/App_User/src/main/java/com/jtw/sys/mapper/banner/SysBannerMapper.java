package com.jtw.sys.mapper.banner;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.banner.TSysBanner;
import com.jtw.sys.vo.banner.SysBannerVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 14:01
 */
public interface SysBannerMapper extends MyMapper<TSysBanner> {
    @Select("SELECT * FROM `t_sys_banner` ${whereSQL}")
    Page<TSysBanner> findAll(@Param("whereSQL") String whereSQL);

    @Select("SELECT * FROM `t_sys_banner` WHERE `TYPE` = 1 AND `STATE` = 1")
    List<SysBannerVo> getAllMobileClientBanner();
}
