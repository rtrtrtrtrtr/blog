package com.jtw.sys.model.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:会员资产变更记录表
 *
 * @author cjsky666
 * @date 2019/1/18 23:02
 */
@Data
@Table(name = "`t_sys_member_asset_log`")
public class TSysMemberAssetLog implements Serializable {
    private static final long serialVersionUID = 2778740071306190099L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Long userId;
    private String serialNo;
    private Integer type;
    private Integer balance;
    private Integer assetBalance;
    private Integer assetFreezeBalance;
    private String descript;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
