package com.jtw.sys.mapper.user;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.user.TSysRole;
import com.jtw.sys.vo.role.SysRoleUpdateVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * DESCRIPT:系统角色DAO
 *
 * @author cjsky666
 * @date 2018/9/14 15:35
 */
public interface SysRoleMapper extends MyMapper<TSysRole> {
    @Select("SELECT `ID`,`NAME`,`DESC` FROM `t_sys_role`")
    Page<SysRoleUpdateVo> findAllRoles();
    @Select("SELECT COUNT(0) FROM `t_sys_role` WHERE ID = #{id};")
    boolean isSysRoleExisting(Integer id);
    @Delete("DELETE FROM `ct_sys_user_role` WHERE ROLE_ID = #{roleId};")
    int deleteRoleWithUser(Integer roleId);
    @Delete("DELETE FROM `ct_sys_role_perm` WHERE ROLE_ID = #{roleId};")
    int deleteRoleWithPerm(Integer roleId);
    @Delete("DELETE FROM `ct_sys_user_role` WHERE `USER_ID` = #{userId} AND `ROLE_ID` IN ${ids}")
    void delAllAgentRolesByUserId(@Param("userId") Long userId,@Param("ids") String ids);
}