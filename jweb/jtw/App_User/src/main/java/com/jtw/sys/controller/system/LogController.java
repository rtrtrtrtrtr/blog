package com.jtw.sys.controller.system;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.service.log.SysAccessServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT: 银行卡信息管理
 *
 * @author cjsky666
 * @date 2018/12/19 16:19
 */
@Api(tags = "访问日志管理模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/log")
public class LogController {
    @Autowired
    private SysAccessServiceImpl sysAccessServiceImpl;

    @PermAnno(sort = 9, group = 99)
    @ApiOperation(notes = "系统登陆可用组",value = "查询系统访问日志")
    @GetMapping(value = "/findAllAccessLog")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "ip"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "userName"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "address"),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "createTime", prefix = "`t_sys_access_log`."),
            })
    public Message findAll(@Valid FindPagingVo findPagingVo){
        return ReqCode.SelectSuccess.getMessage( sysAccessServiceImpl.findAll(findPagingVo));
    }

    @PermAnno(sort = 10, group = 99)
    @ApiOperation(notes = "系统登陆可用组",value = "清除系统访问日志")
    @PostMapping(value = "/delAccessLogBeforeDay")
    public Message delAccessLogBeforeDay(@NotNull(message = "天数不能为空") Integer day){
        sysAccessServiceImpl.delBeforeDay(day);
        return ReqCode.DelSuccess.getMessage();
    }

}
