package com.jtw.sys.model.draw;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:抽奖记录表
 *
 * @author cjsky666
 * @date 2019/7/18 14:20
 */
@Data
@Table(name = "`t_sys_draw_prize_log`")
public class TSysDrawPrizeLog implements Serializable {
    private static final long serialVersionUID = -3460819321262902321L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer userId;
    private Integer prizeLogId;
    private Integer prizeId;
    private Integer drawId;
    private String prizeName;
    private String prizeNickName;
    private Integer total;
    private String image;
    private String ThumbNailImage;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
