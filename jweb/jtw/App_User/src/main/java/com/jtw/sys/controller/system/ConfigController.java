package com.jtw.sys.controller.bank;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.model.system.TSysConfig;
import com.jtw.sys.service.banner.SysBannerServiceImpl;
import com.jtw.sys.service.system.ConfigServiceImpl;
import com.jtw.sys.vo.system.SysConfigAddReq;
import com.jtw.sys.vo.system.SysConfigUpdateReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * DESCRIPT: 银行卡信息管理
 *
 * @author cjsky666
 * @date 2018/12/19 16:19
 */
@Api(tags = "银行卡信息管理模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/config")
public class ConfigController {
    @Autowired
    private SysBannerServiceImpl sysBannerServiceImpl;
    @Autowired
    private ConfigServiceImpl configServiceImpl;

    @ApiOperation(notes = "系统配置项管理",value = "系统配置添加操作")
    @PostMapping(value="/addConfig")
    @PermAnno(sort = 1 , group=10)
    public Message addConfig(HttpServletRequest request, @Valid SysConfigAddReq sysConfigAddReq){
        TSysConfig tSysConfig = new TSysConfig();
        BeanUtils.copyProperties(sysConfigAddReq,tSysConfig);
        tSysConfig.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysConfig.setOperatorIp(request.getRemoteAddr());
        configServiceImpl.save(tSysConfig);
        return ReqCode.AddSuccess.getMessage();
    }

    @ApiOperation(notes = "系统配置项管理",value = "系统配置修改操作")
    @PostMapping(value="/modifyConfigById")
    @PermAnno(sort = 2 , group=10)
    public Message modifyConfigById(HttpServletRequest request,@Valid SysConfigUpdateReq sysConfigUpdateReq){
        TSysConfig tSysConfig = new TSysConfig();
        BeanUtils.copyProperties(sysConfigUpdateReq,tSysConfig);
        tSysConfig.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysConfig.setOperatorIp(request.getRemoteAddr());
        configServiceImpl.update(tSysConfig);
        Constant.sysConfMap = configServiceImpl.getAllConfig();
        return ReqCode.UpdateSuccess.getMessage();
    }

    @PermAnno(sort = 3, group = 10)
    @ApiOperation(notes = "系统配置项管理",value = "查询所有系统配置")
    @GetMapping(value = "/findAllSysConfig")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "keyName"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "keyValue"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "descript"),
            })
    public Message findAllSysConfig(@Valid FindPagingVo findPagingVo){
        return ReqCode.SelectSuccess.getMessage(configServiceImpl.findAll(findPagingVo));
    }

    @PermAnno(sort = 9, group = 1,open = true)
    @ApiOperation(notes = "系统公开组",value = "获取系统配置信息")
    @GetMapping(value = "/getAllSysConfig")
    public Message getAllSysConfig(){
        if(Constant.sysConfMap.get(Constant.KEY_SYSTEM_VERSION)==null){
            Constant.sysConfMap.put(Constant.KEY_SYSTEM_VERSION,Constant.KEY_SYSTEM_VERSION_VALUE);
        }
        return ReqCode.SelectSuccess.getMessage(Constant.sysConfMap);
    }

    @PermAnno(sort = 4, group = 13)
    @ApiOperation(notes = "客户端配置管理",value = "保存移动客户端背景图")
    @PostMapping(value = "/saveClientBgImageInfo")
    public Message saveClientBgImageInfo(HttpServletRequest request,@NotBlank(message = "图片路径不能为空") String bgimg){
        Constant.sysConfMap.put(Constant.KEY_CLIENT_BG_IMAGE,bgimg);
        TSysConfig tSysConfig = configServiceImpl.getByKeyName(Constant.KEY_CLIENT_BG_IMAGE);
        tSysConfig.setKeyValue(bgimg);
        if(tSysConfig==null){
            tSysConfig = new TSysConfig();
            tSysConfig.setKeyName(Constant.KEY_CLIENT_BG_IMAGE);
            configServiceImpl.save(tSysConfig);
        }else{
            tSysConfig.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
            tSysConfig.setOperatorIp(request.getRemoteAddr());
            configServiceImpl.update(tSysConfig);
        }
        return ReqCode.UpdateSuccess.getMessage(Constant.sysConfMap);
    }

    @PermAnno(sort = 5, group = 13)
    @ApiOperation(notes = "客户端配置管理",value = "保存PC客户端背景图")
    @PostMapping(value = "/saveClientBgImageInfoPC")
    public Message saveClientBgImageInfoPC(HttpServletRequest request,@NotBlank(message = "图片路径不能为空") String bgimg){
        Constant.sysConfMap.put(Constant.KEY_CLIENT_BG_IMAGE_PC,bgimg);
        TSysConfig tSysConfig = configServiceImpl.getByKeyName(Constant.KEY_CLIENT_BG_IMAGE_PC);
        tSysConfig.setKeyValue(bgimg);
        if(tSysConfig==null){
            tSysConfig = new TSysConfig();
            tSysConfig.setKeyName(Constant.KEY_CLIENT_BG_IMAGE_PC);
            configServiceImpl.save(tSysConfig);
        }else{
            tSysConfig.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
            tSysConfig.setOperatorIp(request.getRemoteAddr());
            configServiceImpl.update(tSysConfig);
        }
        return ReqCode.UpdateSuccess.getMessage(Constant.sysConfMap);
    }
    @PermAnno(sort = 10, group = 1,open = true)
    @ApiOperation(notes = "系统公开组",value = "获取手机端轮播图列表")
    @GetMapping(value = "/getAllMobileClientBanner")
    public Message getAllMobileClientBanner(){
        return ReqCode.SelectSuccess.getMessage(sysBannerServiceImpl.getAllMobileClientBanner());
    }

    @PermAnno(sort = 4, group = 13)
    @ApiOperation(notes = "客户端配置管理",value = "保存网站LOGO")
    @PostMapping(value = "/saveWebLogo")
    public Message saveWebLogo(HttpServletRequest request,@NotBlank(message = "图片路径不能为空") String webLogo){
        Constant.sysConfMap.put(Constant.KEY_WEB_LOGO,webLogo);
        TSysConfig tSysConfig = configServiceImpl.getByKeyName(Constant.KEY_WEB_LOGO);
        tSysConfig.setKeyValue(webLogo);
        if(tSysConfig==null){
            tSysConfig = new TSysConfig();
            tSysConfig.setKeyName(Constant.KEY_WEB_LOGO);
            configServiceImpl.save(tSysConfig);
        }else{
            tSysConfig.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
            tSysConfig.setOperatorIp(request.getRemoteAddr());
            configServiceImpl.update(tSysConfig);
        }
        return ReqCode.UpdateSuccess.getMessage(Constant.sysConfMap);
    }
}
