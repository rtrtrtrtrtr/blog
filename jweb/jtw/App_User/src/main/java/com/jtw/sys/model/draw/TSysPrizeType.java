package com.jtw.sys.model.draw;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:奖品类别库
 *
 * @author cjsky666
 * @date 2019/7/18 13:47
 */
@Data
@Table(name = "`t_sys_prize_type`")
public class TSysPrizeType implements Serializable {
    private static final long serialVersionUID = 9086206401930454450L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String name;
    private String nickName;
    private String image;
    private String ThumbNailImage;
    private Integer type;
    private String operatorIp;
    private String operator;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;


}
