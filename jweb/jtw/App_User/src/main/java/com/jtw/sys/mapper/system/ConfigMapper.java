package com.jtw.sys.mapper.system;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.system.TSysConfig;
import com.jtw.sys.vo.system.SysConfigCustomVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:26
 */
public interface ConfigMapper extends MyMapper<TSysConfig> {
    @Select("SELECT * FROM `t_sys_config` ${whereSQL}")
    Page<TSysConfig> findAll(@Param("whereSQL") String whereSQL);

    @Select("SELECT `KEY_NAME`,`KEY_VALUE` FROM `t_sys_config`")
    List<SysConfigCustomVo> getAllConfig();

    @Select("SELECT * FROM `t_sys_config` WHERE `KEY_NAME` = #{keyName}")
    TSysConfig getByKeyName(String keyName);
}
