package com.jtw.sys.mapper.member;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.member.TSysMemberCheckIn;
import com.jtw.sys.vo.member.MemberCheckInVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/8 11:22
 */
public interface MemberCheckInMapper extends MyMapper<TSysMemberCheckIn> {
    @Select("SELECT COUNT(0) FROM `t_sys_member_check_in` WHERE `USER_ID` = #{userId} AND CHECK_IN_DATE = #{currentDate}")
    Integer getTodayCheckIn(@Param("userId") Long userId, @Param("currentDate") Date currentDate);
    @Select("SELECT `t_sys_member_check_in`.`ID`," +
            " `t_sys_member_check_in`.`USER_NAME`," +
            " `t_sys_member_check_in`.`CHECK_IN_DATE`," +
            " `t_sys_member_check_in_reward`.`CHECK_IN_REWARD_BALANCE`," +
            " `t_sys_member_check_in_reward`.`STATE`" +
            " FROM `t_sys_member_check_in` LEFT JOIN `t_sys_member_check_in_reward`" +
            " ON `t_sys_member_check_in`.`ID` = `t_sys_member_check_in_reward`.`CHECK_IN_ID`" +
            " ${whereSQL}")
    Page<MemberCheckInVo> findAllCheckInReward(@Param("whereSQL") String whereSQL);
}
