package com.jtw.sys.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/20 11:47
 */
@Data
public class UserWithdrawPwdResetVo implements Serializable {
    private static final long serialVersionUID = 7211830609729274668L;
    private Long id;
    private String userName;
    private String reason;
    private Integer state;
    private String descript;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
