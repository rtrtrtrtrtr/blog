package com.jtw.sys.model.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:用户基本信息
 * id与用户表同相同
 *
 * @author cjsky666
 * @date 2018/9/14 14:48
 */
@Data
@Table(name = "`t_sys_user_info`")
public class TSysUserInfo implements Serializable {
    private static final long serialVersionUID = 3012372906431003778L;
    @Id
    private Long id;
    private String desc;
    private String nickName;
    private String headImage;
    private String thumbNailImage;
    private Integer sex;
    private Integer age;
    private String mobile;
    private String email;
    private String qq;
    private Date lastLoginTime;
    private Date currentLoginTime;
    private String address;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
