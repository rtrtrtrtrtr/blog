package com.jtw.sys.model.draw;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:抽奖活动奖品发放顺序表
 * @author cjsky666
 * @date 2019/7/25 10:52
 */
@Data
@Table(name="`t_sys_draw_prize_order`")
public class TSysDrawPrizeOrder implements Serializable {
    private static final long serialVersionUID = 418960563607818692L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer drawId;
    private Integer drawPrizeId;
    private Integer threshold;//派发阀值，当日抽奖的用户数量超过这个阀值比例，实物奖品还没有被派发的话，就直接派发。以免出现没有人抽中实物的情况。
    private Integer appointType;//派发指定类型：1、只按数量2、按日期和数量'
    private Integer num;//可获取的个数 -1 为无限制。
    private Integer handNum;//已派发的个数
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date obtainTime;//可获取的日期
    private String operator;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
