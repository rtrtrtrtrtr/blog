package com.jtw.sys.model.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 签到奖励详情表
 *
 * @author cjsky666
 * @date 2019/7/8 10:06
 */
@Data
@Table(name = "`t_sys_member_check_in_reward`")
public class TSysMemberCheckInReward implements Serializable {
    private static final long serialVersionUID = 7086884072834237722L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer checkInId;
    private Integer checkInRewardBalance;
    private Integer state;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
