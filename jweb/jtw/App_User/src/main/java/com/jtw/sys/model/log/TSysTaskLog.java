package com.jtw.sys.model.log;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 14:26
 */
@Data
@Table(name = "`t_sys_task_log`")
public class TSysTaskLog implements Serializable {
    private static final long serialVersionUID = 3983428841902726584L;
    @Id
    private Long id;//主键
    private Integer taskId;//计划任务id
    private Integer gameId;//游戏id
    private String name;//计划任务名称
    private String descript;//描述
    private Boolean state;//任务状态
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;//执行时间
}
