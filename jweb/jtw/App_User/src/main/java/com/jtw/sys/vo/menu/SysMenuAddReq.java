package com.jtw.sys.vo.menu;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/10 12:03
 */
@Data
public class SysMenuAddReq implements Serializable {
    private static final long serialVersionUID = -8244809096316289309L;
    @NotBlank(message = "名称不能为空")
    private String name;
    @NotNull(message = "父菜单id不能为空")
    private Integer parent;
    private String desc;
    @NotNull(message = "菜单类型不能为空")
    private Integer type;
    private Integer group;
    private Integer sort;
    private String path;
    private Boolean open;
}
