package com.jtw.sys.model.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:第三方平台授权表
 *
 * @author cjsky666
 * @date 2019/7/24 23:13
 */
@Data
@Table(name = "`t_sys_user_open`")
public class TSysUserOpen implements Serializable {
    private static final long serialVersionUID = 4984630704107541469L;
    private Integer id;
    private Integer userId;
    private String wxGzhOpenId;
    private String wxAppOpenId;
    private String qqOpenId;
    private String zfbOpenId;
    private String txUnionOpenId;
    private String sinaOpenId;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
