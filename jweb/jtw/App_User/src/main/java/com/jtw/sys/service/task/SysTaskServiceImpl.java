package com.jtw.sys.service.task;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.mapper.task.SysTaskMapper;
import com.jtw.sys.model.task.TSysTask;
import com.jtw.sys.vo.task.SysTaskAddReq;
import com.jtw.sys.vo.task.TaskModelVo;
import com.jtw.sys.util.TaskUtil;
import org.apache.shiro.SecurityUtils;
import org.quartz.SchedulerException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 10:07
 */
@Service
public class SysTaskServiceImpl implements SysTaskService {
    @Autowired
    private SysTaskMapper sysTaskMapper;

    @Override
    public void save(TSysTask tSysTask) {
        tSysTask.setRunState(true);
        tSysTask.setUpdateId((Long) SecurityUtils.getSubject().getSession().getAttribute(Constant.USER_PRIMARY_KEY));
        tSysTask.setCreateId((Long) SecurityUtils.getSubject().getSession().getAttribute(Constant.USER_PRIMARY_KEY));
        tSysTask.setId(sysTaskMapper.insertSelective(tSysTask));
        if(tSysTask.getTaskState()){
            TaskModelVo taskModelVo = new TaskModelVo();
            BeanUtils.copyProperties(tSysTask, taskModelVo);
            TaskUtil.addTask(taskModelVo);
        }
    }

    @Override
    public void saveAll(List<TSysTask> list) {

    }

    @Override
    @Transactional
    public void update(TSysTask tSysTask)  {
        //判断是否存在
        TSysTask oldsysTask  = sysTaskMapper.selectByPrimaryKey(tSysTask.getId());
        if(oldsysTask==null){
            throw ReqCode.UnExistingDataException.getException();
        }
        tSysTask.setUpdateId((Long) SecurityUtils.getSubject().getSession().getAttribute(Constant.USER_PRIMARY_KEY));
        //存在就更新
        sysTaskMapper.updateByPrimaryKeySelective(tSysTask);

        //移除旧任务
        TaskModelVo oldtaskModelVo = new TaskModelVo();
        BeanUtils.copyProperties(oldsysTask, oldtaskModelVo);
        try {
            TaskUtil.removeTask(oldtaskModelVo);//移除旧的
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        //是否需要启动更新后的任务
        if(tSysTask.getTaskState()){
            TaskModelVo taskModelVo = new TaskModelVo();
            BeanUtils.copyProperties(tSysTask, taskModelVo);
            TaskUtil.addTask(taskModelVo);
        }

    }

    @Override
    public void delete(Object id) {

    }
    @Override
    public TSysTask getById(Object id) {
        return sysTaskMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<TSysTask> findAll(PagingVo pagingVo) {
        return null;
    }

    public Page<TSysTask> findAll(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return sysTaskMapper.findAll(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    public List<TSysTask> getAll() {
        return sysTaskMapper.selectAll();
    }
}
