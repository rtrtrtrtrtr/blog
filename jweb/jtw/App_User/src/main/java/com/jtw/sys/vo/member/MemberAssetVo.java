package com.jtw.sys.vo.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 用户资产信息业务类
 *
 * @author cjsky666
 * @date 2019/1/19 20:52
 */
@Data
public class MemberAssetVo implements Serializable {
    private static final long serialVersionUID = -7634161671603332167L;
    private Long id;
    private Integer balance;
    private Integer freezeBalance;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
