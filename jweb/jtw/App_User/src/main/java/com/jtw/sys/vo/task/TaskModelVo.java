package com.jtw.sys.vo.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jtw.sys.model.task.TSysTask;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:定时任务启动模型
 *
 * @author cjsky666
 * @date 2018/11/23 15:54
 */
@Data
public class TaskModelVo extends TSysTask implements Serializable {
    private static final long serialVersionUID = -8442080947020826369L;
    @NotNull(message = "任务id不能为空")
    private Integer id;
    @NotNull(message = "任务的游戏id不能为空")
    private Integer gameId;
    @NotBlank(message = "任务名称不能为空")
    private String name;
    @NotBlank(message = "任务组名称不能为空")
    private String groupName;
    @NotBlank(message = "任务周期不能为空")
    private String cron;
    @NotNull(message = "任务状态不能为空")
    private Boolean taskState;
    @NotBlank(message = "任务参数不能为空")
    private String param;
    @NotBlank(message = "任务执行方法不能为空")
    private String methodName;
    @NotBlank(message = "任务描述不能为空")
    private String descript;
    @NotBlank(message = "任务类名不能为空")
    private String beanClass;
    private Long createId;
    private Long updateId;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    private String springId;
}
