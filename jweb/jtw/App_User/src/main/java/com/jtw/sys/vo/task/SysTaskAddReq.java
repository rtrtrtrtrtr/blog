package com.jtw.sys.vo.task;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 10:19
 */
@Data
public class SysTaskAddReq implements Serializable {
    private static final long serialVersionUID = -8239912199862576250L;
    @NotBlank(message = "任务名称不能为空")
    private String name;
    @NotBlank(message = "任务组名称不能为空")
    private String groupName;
    @NotBlank(message = "任务周期不能为空")
    private String cron;
    @NotNull(message = "任务状态不能为空")
    private Boolean taskState;
    @NotNull(message = "任务的游戏id不能为空")
    private Integer gameId;
    @NotBlank(message = "任务参数不能为空")
    private String param;
    @NotBlank(message = "任务执行方法不能为空")
    private String methodName;
    @NotBlank(message = "任务描述不能为空")
    private String descript;
    @NotBlank(message = "任务类名不能为空")
    private String beanClass;
}
