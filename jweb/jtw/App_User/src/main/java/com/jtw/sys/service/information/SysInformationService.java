package com.jtw.sys.service.information;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.model.information.TSysInformation;
import com.jtw.sys.vo.information.SysInformationLessVo;
import com.jtw.sys.vo.information.SysInformationVo;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:49
 */
public interface SysInformationService extends BaseService<TSysInformation> {
    SysInformationVo getInformationById(Integer id);

    Page<SysInformationLessVo> findAllSysInformation(FindPagingVo findPagingVo);
}
