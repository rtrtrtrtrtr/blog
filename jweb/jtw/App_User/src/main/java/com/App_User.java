package com;

import io.swagger.annotations.Api;
import org.joda.time.DateTime;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.boot.web.servlet.ServletComponentScan;
import tk.mybatis.spring.annotation.MapperScan;
@SpringBootApplication
//@EnableEurekaClient
@Api(tags = "管理后台接口文档")
//开启redis session 集群
//@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 3600)
@MapperScan("com.jtw.sys.mapper")
public class App_User {
    public static void main(String[] args) {
        SpringApplication.run(App_User.class, args);
    }
}

