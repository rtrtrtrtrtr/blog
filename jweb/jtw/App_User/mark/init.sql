/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 08/08/2019
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ct_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_role_menu`;
CREATE TABLE `ct_sys_role_menu`  (
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `MENU_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ROLE_ID`, `MENU_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色-菜单关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ct_sys_role_perm
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_role_perm`;
CREATE TABLE `ct_sys_role_perm`  (
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `PERM_ID` int(11) UNSIGNED NOT NULL COMMENT '权限ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ROLE_ID`, `PERM_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色-权限关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ct_sys_user_admin
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_user_admin`;
CREATE TABLE `ct_sys_user_admin`  (
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理员账号-名单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ct_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_user_role`;
CREATE TABLE `ct_sys_user_role`  (
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`USER_ID`, `ROLE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统用户-角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_access_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_access_log`;
CREATE TABLE `t_sys_access_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id,有则记录，没有为0',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的用户名，没有则为空字符串',
  `URL_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的URL名称',
  `IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户访问的ip地址',
  `PORT` int(10) UNSIGNED NOT NULL COMMENT '访问端口',
  `URL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的URL路径',
  `ADDRESS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ip定位地址',
  `MAC` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '客户端的MAC地址，此数据不成立，所有查看到的实现方式都是错误的，过时的。不是真正的mac地址',
  `PARAMS` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '访问参数',
  `HTTP_METHOD` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '接口调用类型',
  `OS` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作系统',
  `RENDER_ENGINE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '渲染引擎标志',
  `BROWSER_VERSION` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '浏览器版本',
  `BROWSER_TYPE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '访问设备类型',
  `BROWSER_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '访问设备名称',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '记录时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '访问日志记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_banner`;
CREATE TABLE `t_sys_banner`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `IMAGE_PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '高清图片',
  `THUMB_NAIL_IMAGE_PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '缩略图片',
  `URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  `SKIP_WAY` int(2) NOT NULL DEFAULT 0 COMMENT '跳转方式：0不跳转。1跳转',
  `TYPE` int(2) NOT NULL DEFAULT 1 COMMENT 'banner类型：1为app端，2为电脑端',
  `STATE` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0待审核，1已审核,2已禁用',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统banner设置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config`  (
  `ID` int(4) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `KEY_NAME` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key名称',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key的中文描述信息',
  `KEY_VALUE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key的值',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者用户名',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  PRIMARY KEY (`ID`, `OPERATOR`) USING BTREE,
  UNIQUE INDEX `KEY_NAME`(`KEY_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统设置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_config
-- ----------------------------
INSERT INTO `t_sys_config` VALUES (1, 'webName', '网站名称（显示于客户端,后台登陆、管理界面）', '告诉你', 'admin', '0:0:0:0:0:0:0:1', '2019-08-16 09:57:00.320', '2019-01-18 18:00:04.202');
INSERT INTO `t_sys_config` VALUES (2, 'serviceLocked', '网站会员访问锁定(1代表锁定，其他不锁定)', '0', 'admin', '127.0.0.1', '2019-02-19 12:03:19.897', '2019-01-18 23:52:45.809');
INSERT INTO `t_sys_config` VALUES (3, 'customServiceLink', '客服链接', '', 'admin', '115.84.98.46', '2019-07-17 09:13:36.429', '2019-01-19 00:30:15.893');
INSERT INTO `t_sys_config` VALUES (4, 'friendlyReminder', '温馨提示', '', 'admin', '117.20.115.131', '2019-07-17 09:13:45.021', '2019-01-20 23:43:16.252');
INSERT INTO `t_sys_config` VALUES (5, 'companyName', '公司标志', 'Copyright @2019-2020 告诉你. All Rights Reserved', 'admin', '0:0:0:0:0:0:0:1', '2019-08-16 09:56:23.646', '2019-01-20 23:44:06.416');
INSERT INTO `t_sys_config` VALUES (6, 'serviceLockedInfo', '网站访问锁定提示信息', '服务器升级，暂停访问', 'admin', '117.20.115.131', '2019-07-17 09:15:17.988', '2019-01-22 11:49:12.959');
INSERT INTO `t_sys_config` VALUES (7, 'clientBgImage', '客户端背景图', '/static/image/otherImage/20190715/8b2d7237c1f14d4f8b3708085a0f8749.jpg', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:09.184', '2019-02-18 10:24:56.348');
INSERT INTO `t_sys_config` VALUES (8, 'noticeBarId', '首页通告栏的资讯id（通告栏滚动只显示标题，但是可以打开内容详情）', '', 'admin', '27.109.113.247', '2019-08-05 17:37:12.262', '2019-02-25 17:06:31.704');
INSERT INTO `t_sys_config` VALUES (9, 'webLogo', '网站logo图标', '/static/image/otherImage/20190815/3364be00456f4a7c9ec5f01f9ab3377a.png', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:12.868', '2019-03-04 11:03:49.793');
INSERT INTO `t_sys_config` VALUES (10, 'clientBgImagePC', '网站PC客户端端背景图', '/static/image/otherImage/20190815/12421be93c8b4597afd27fc934f773ec.jpg', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:14.468', '2019-05-05 17:36:11.831');
INSERT INTO `t_sys_config` VALUES (11, 'checkInReward', '每日签到积分奖励', '50', 'admin', '0:0:0:0:0:0:0:1', '2019-07-21 22:41:52.980', '2019-07-21 22:41:49.357');
INSERT INTO `t_sys_config` VALUES (12, 'checkInCount', '每日可签到次数', '1', 'admin', '0:0:0:0:0:0:0:1', '2019-07-21 22:42:25.390', '2019-07-21 22:42:25.390');

-- ----------------------------
-- Table structure for t_sys_information
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_information`;
CREATE TABLE `t_sys_information`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `AUTHOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `TITLE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `STATE` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0待审核，1已审核,2已禁用',
  `CONTENT` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章内容',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '简介',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统资讯表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_asset
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_asset`;
CREATE TABLE `t_sys_member_asset`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户主键id',
  `BALANCE` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户余额',
  `FREEZE_BALANCE` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账户余额冻结',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `UPDATE_TIME`(`UPDATE_TIME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员资产表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_asset_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_asset_log`;
CREATE TABLE `t_sys_member_asset_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户主键id',
  `SERIAL_NO` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '账变记录对应的流水号,后续可以根据账变类型去查相应的触发记录',
  `TYPE` int(2) UNSIGNED NOT NULL COMMENT '变更类型：0、新建记录',
  `BALANCE` int(11) NOT NULL COMMENT '产生的余额数值：增加为正整数。减少为负数',
  `ASSET_BALANCE` int(11) NOT NULL COMMENT '变更后的余额数值',
  `ASSET_FREEZE_BALANCE` int(11) NOT NULL COMMENT '变更后的冻结余额数值',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账变描述信息',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者用户名',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`, `TYPE`, `SERIAL_NO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员资产变更记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_check_in
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_check_in`;
CREATE TABLE `t_sys_member_check_in`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `USER_ID` int(11) NOT NULL COMMENT '用户id',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `CHECK_IN_DATE` date NOT NULL COMMENT '签到日期',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `USER_ID`(`USER_ID`, `CHECK_IN_DATE`) USING BTREE,
  INDEX `CHECK_IN_DATE`(`CHECK_IN_DATE`) USING BTREE,
  INDEX `CREATE_TIME`(`CREATE_TIME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统会员签到表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_check_in_reward
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_check_in_reward`;
CREATE TABLE `t_sys_member_check_in_reward`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `CHECK_IN_ID` int(11) UNSIGNED NOT NULL COMMENT '签到id',
  `CHECK_IN_REWARD_BALANCE` int(11) NOT NULL COMMENT '签到奖励金额',
  `STATE` tinyint(1) NOT NULL DEFAULT 0 COMMENT '奖励发放状态:0,待发放，1已发放，2，已拒绝',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作人',
  `OPERATOR_IP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作ip',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `CHECK_IN_ID`(`CHECK_IN_ID`) USING BTREE,
  INDEX `CHECK_IN_REWARD_BALANCE`(`CHECK_IN_REWARD_BALANCE`) USING BTREE,
  INDEX `STATE`(`STATE`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统会员签到奖励详情表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_pwd_reset
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_pwd_reset`;
CREATE TABLE `t_sys_member_pwd_reset`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '表主键',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `REASON` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '申请原因',
  `STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请状态：0为待处理,1已处理,2已拒绝',
  `DESCRIPT` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '处理结果',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_NAME`(`USER_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '密码重置申请表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `PARENT` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `DESC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  `TYPE` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '菜单类型：1为根菜单，2为子菜单',
  `GROUP` tinyint(4) NOT NULL DEFAULT 0 COMMENT '分组',
  `SORT` int(4) NOT NULL DEFAULT 0 COMMENT '排序',
  `PATH` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单路径',
  `OPEN` tinyint(4) NOT NULL DEFAULT 0 COMMENT '菜单是否为公开',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `PARENT`(`PARENT`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_menu
-- ----------------------------
INSERT INTO `t_sys_menu` VALUES (1, '用户管理', 0, '用户管理菜单', 1, 1, 1, '', 0, '', '', '2018-10-25 16:15:51.998', '2018-10-25 16:15:51.998');
INSERT INTO `t_sys_menu` VALUES (2, '管理员', 1, '平台管理人员账号', 2, 1, 2, '/sys/users/findAll', 0, '', '', '2018-10-25 16:16:24.331', '2019-01-11 09:05:47.373');
INSERT INTO `t_sys_menu` VALUES (3, '系统管理', 0, '系统管理菜单', 1, 2, 6, '', 0, '', '', '2018-10-25 16:16:58.224', '2019-03-16 02:45:30.902');
INSERT INTO `t_sys_menu` VALUES (5, '权限列表', 3, '查看所有权限列表', 2, 3, 2, '/sys/perms/findAll', 0, '', '', '2018-10-25 16:18:22.611', '2019-07-17 08:45:10.888');
INSERT INTO `t_sys_menu` VALUES (7, '菜单列表', 3, '系统菜单列表', 2, 3, 4, '/sys/menus/findAll', 0, '', '', '2018-10-25 16:19:19.863', '2019-07-17 08:48:49.415');
INSERT INTO `t_sys_menu` VALUES (9, '角色列表', 3, '角色列表菜单', 2, 4, 1, '/sys/roles/findAll', 0, '', '', '2018-10-25 16:20:49.769', '2019-07-17 08:45:40.127');
INSERT INTO `t_sys_menu` VALUES (11, '定时任务', 3, '定时任务列表', 2, 5, 2, '/sys/task/findAll', 0, '', '', '2018-11-23 12:46:24.694', '2019-07-17 08:45:48.399');
INSERT INTO `t_sys_menu` VALUES (19, '注册会员', 1, '注册用户列表', 2, 1, 1, '/sys/members/findAll', 0, '', '', '2019-01-11 09:02:45.397', '2019-01-11 09:06:17.179');
INSERT INTO `t_sys_menu` VALUES (20, '初始化配置', 3, '系统初始化配置', 2, 4, 5, '/sys/system/findAll', 0, '', '', '2019-01-18 17:20:04.950', '2019-07-17 08:44:41.780');
INSERT INTO `t_sys_menu` VALUES (23, '会员功能', 0, '会员功能菜单', 1, 1, 2, '', 0, '', '', '2019-02-01 23:50:16.281', '2019-03-16 02:45:09.982');
INSERT INTO `t_sys_menu` VALUES (32, '客户端设置', 0, '客户端设置', 1, 6, 7, '', 0, '', '', '2019-02-18 10:09:40.336', '2019-03-16 02:45:34.273');
INSERT INTO `t_sys_menu` VALUES (33, '背景图更新', 32, '背景图更新', 2, 1, 1, '/sys/client/bgImg', 0, '', '', '2019-02-18 10:10:13.275', '2019-02-18 10:10:13.275');
INSERT INTO `t_sys_menu` VALUES (34, '登陆密码重置', 23, '用户登陆密码重置', 2, 4, 7, '/sys/member/pwdResetApply', 0, '', '', '2019-02-19 09:07:09.613', '2019-07-22 10:12:51.310');
INSERT INTO `t_sys_menu` VALUES (37, '轮播图管理', 32, '轮播图列表', 2, 1, 2, '/sys/client/banner', 0, '', '', '2019-02-21 16:27:42.617', '2019-02-21 16:27:42.617');
INSERT INTO `t_sys_menu` VALUES (38, '资讯管理', 0, '资讯公告管理', 1, 1, 8, '', 0, '', '', '2019-02-21 16:30:22.262', '2019-03-16 02:45:39.118');
INSERT INTO `t_sys_menu` VALUES (39, '资讯列表', 38, '系统资讯列表', 2, 1, 1, '/sys/information/findAll', 0, '', '', '2019-02-21 16:31:15.668', '2019-02-21 16:31:15.668');
INSERT INTO `t_sys_menu` VALUES (40, '网站LOGO', 32, '更新客户端logo', 2, 1, 3, '/sys/client/webLogo', 0, '', '', '2019-03-04 23:28:10.982', '2019-03-04 23:29:07.124');
INSERT INTO `t_sys_menu` VALUES (44, '日志管理', 0, '日志管理菜单', 1, 1, 9, '', 0, '', '', '2019-03-15 13:14:05.155', '2019-03-16 02:46:22.270');
INSERT INTO `t_sys_menu` VALUES (45, '用户访问日志', 44, '访问日志', 2, 1, 1, '/sys/log/findAllAccessLog', 0, '', '', '2019-03-15 13:14:57.709', '2019-03-15 13:14:57.709');
INSERT INTO `t_sys_menu` VALUES (46, '系统日志文件', 44, '本地日志文件列表', 2, 1, 2, '/sys/log/getAllSysLogFile', 0, '', '', '2019-05-22 09:02:38.307', '2019-05-22 09:02:38.307');
INSERT INTO `t_sys_menu` VALUES (56, '积分管理', 0, '积分管理菜单', 1, 1, 1, '', 0, '', '', '2019-07-22 10:12:32.454', '2019-07-22 10:12:32.454');
INSERT INTO `t_sys_menu` VALUES (57, '会员积分', 56, '会员账户积分', 2, 1, 1, '/sys/member/asset', 0, '', '', '2019-07-22 10:13:15.954', '2019-07-22 10:13:15.954');
INSERT INTO `t_sys_menu` VALUES (58, '积分记录', 56, '积分使用记录', 2, 1, 2, '/sys/member/assetChangeLog', 0, '', '', '2019-07-22 10:13:30.636', '2019-07-22 10:13:30.636');
INSERT INTO `t_sys_menu` VALUES (60, '签到记录', 23, '会员签到记录', 2, 1, 2, '/sys/member/checkInLog', 0, '', '', '2019-07-23 11:41:40.587', '2019-07-23 11:42:42.054');

-- ----------------------------
-- Table structure for t_sys_perm
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_perm`;
CREATE TABLE `t_sys_perm`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限ID主键',
  `NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名称',
  `DESC` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '权限描述',
  `HTTP_METHOD` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'GET' COMMENT 'Http权限请求类型',
  `TYPE` tinyint(4) UNSIGNED NOT NULL COMMENT '权限类型 1、菜单2、按钮、3其他',
  `GROUP` tinyint(4) UNSIGNED NOT NULL COMMENT '所属的权限组：',
  `URL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统权限请求路径',
  `SORT` int(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限在当前模块下的顺序',
  `CLASS_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '接口所在的系统包名',
  `METHOD_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类方法名称',
  `OPEN` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是公开权限',
  `STATE` tinyint(3) UNSIGNED NOT NULL DEFAULT 2 COMMENT '状态:默认为1,0为禁用,2,为登陆可用',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`) USING BTREE,
  UNIQUE INDEX `URL`(`URL`) USING BTREE,
  INDEX `GROUP`(`GROUP`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_perm_group
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_perm_group`;
CREATE TABLE `t_sys_perm_group`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限组ID主键',
  `GROUP` tinyint(2) UNSIGNED NOT NULL COMMENT '权限分组ID',
  `DESC` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '默认:1、公开组2、菜单组3、权限组4、角色组5、系统用户组99、管理用户登陆可用98、会员用户登陆可用',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `GROUP`(`GROUP`, `DESC`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统权限分组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID主键',
  `NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统角色名称',
  `DESC` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '角色描述',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_task
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_task`;
CREATE TABLE `t_sys_task`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `GROUP_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务组',
  `CRON` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务的执行周期，例如:\"0/120 * * * * ?\"',
  `GAME_ID` int(11) UNSIGNED NOT NULL COMMENT '游戏的主键id',
  `TASK_STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务发布状态',
  `RUN_STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务运行状态',
  `PARAM` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务的参数',
  `METHOD_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务调用的方法',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务描述',
  `BEAN_CLASS` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务实例bean的类',
  `CREATE_ID` int(11) UNSIGNED NOT NULL COMMENT '创建着用户id',
  `UPDATE_ID` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`, `GAME_ID`) USING BTREE,
  INDEX `CREATE_ID`(`CREATE_ID`, `UPDATE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统定时任务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_task_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_task_log`;
CREATE TABLE `t_sys_task_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TASK_ID` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `GAME_ID` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务名',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务描述',
  `STATE` tinyint(4) UNSIGNED NOT NULL COMMENT '任务发布状态',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `GAME_ID`(`GAME_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统定时任务日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID主键',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `SALT` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码盐',
  `STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号状态：0为正常,1为锁定,2为删除',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `USER_NAME`(`USER_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_info`;
CREATE TABLE `t_sys_user_info`  (
  `ID` int(11) UNSIGNED NOT NULL COMMENT '用户信息id与用户表相同',
  `NICK_NAME` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户昵称',
  `HEAD_IMAGE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户头像地址',
  `THUMB_NAIL_IMAGE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '头像缩略图地址',
  `SEX` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别:1：男，2：女,0：保密',
  `AGE` int(3) UNSIGNED NULL DEFAULT NULL COMMENT '年龄',
  `MOBILE` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '手机号码',
  `EMAIL` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '邮箱',
  `QQ` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'QQ号码',
  `DESC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户描述信息',
  `LAST_LOGIN_TIME` datetime(3) NULL DEFAULT NULL COMMENT '上次登录时间',
  `CURRENT_LOGIN_TIME` datetime(3) NULL DEFAULT NULL COMMENT '当前登录时间',
  `ADDRESS` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户地址',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `MOBILE`(`MOBILE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_user_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_login_log`;
CREATE TABLE `t_sys_user_login_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '登录日志主键id',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '登录ip地址',
  `ADDRESS` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登陆地址',
  `DEVICE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登陆设备名称',
  `LOGIN_MODE` int(2) NOT NULL DEFAULT 1 COMMENT '登陆方式，1为浏览器，2为App登陆',
  `LOGIN_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '登录时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE,
  INDEX `DEVICE`(`DEVICE`) USING BTREE,
  INDEX `ADDRESS`(`ADDRESS`) USING BTREE,
  INDEX `LOGIN_TIME`(`LOGIN_TIME`) USING BTREE,
  INDEX `IP`(`IP`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户登录信息记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_user_open
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_open`;
CREATE TABLE `t_sys_user_open`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `WX_GZH_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '微信公众平台(web)openid',
  `WX_APP_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '微信开放平台(APP)openid',
  `QQ_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'QQ开放平台openid',
  `ZFB_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '支付宝开放平台openid',
  `TX_UNION_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '腾讯互联平台unionid',
  `SINA_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '新浪微博unionid',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE,
  INDEX `WX_GZH_OPEN_ID`(`WX_GZH_OPEN_ID`) USING BTREE,
  INDEX `TX_UNION_ID`(`TX_UNION_ID`) USING BTREE,
  INDEX `WX_APP_OPEN_ID`(`WX_APP_OPEN_ID`) USING BTREE,
  INDEX `QQ_OPEN_ID`(`QQ_OPEN_ID`) USING BTREE,
  INDEX `ZFB_OPEN_ID`(`ZFB_OPEN_ID`) USING BTREE,
  INDEX `SINA_OPEN_ID`(`SINA_OPEN_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '第三方平台授权扩展表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
