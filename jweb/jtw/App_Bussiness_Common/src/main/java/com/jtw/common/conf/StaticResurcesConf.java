package com.jtw.common.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * DESCRIPT: 自定义静态资源访问路径
 * @author cjsky666
 * @date 2018/11/21 15:53
 */
@Configuration
public class StaticResurcesConf implements WebMvcConfigurer {
    //动态切换需要自定义映射的静态资源路径
    @Value("${webappUploadPath}")
    private String path;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations(path);
//        super.addResourceHandlers(registry);
    }
}