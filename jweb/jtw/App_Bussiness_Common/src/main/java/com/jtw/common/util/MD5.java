package com.jtw.common.util;

import java.security.MessageDigest;

/**
 * MD5加密
 */
public class MD5 {

	private final static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	
    private static String bytesToHex(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        int t;
        for (int i = 0; i < 16; i++) {
            t = bytes[i];
            if (t < 0)
                t += 256;
            sb.append(hexDigits[(t >>> 4)]);
            sb.append(hexDigits[(t % 16)]);
        }
        return sb.toString();
    }
    
    public static String encrypt(String input) {
        return encrypt(input, 32);
    }
    
    public static String encrypt(String input, int bit) {
    	String code = null;
        try {
            MessageDigest md = MessageDigest.getInstance(System.getProperty("MD5.algorithm", "MD5"));
            if (bit == 16) {
            	code = bytesToHex(md.digest(input.getBytes("utf-8"))).substring(8, 24);
            } else {
            	code = bytesToHex(md.digest(input.getBytes("utf-8")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }
    
    public static String md5_3(String b) throws Exception {
        MessageDigest md = MessageDigest.getInstance(System.getProperty("MD5.algorithm", "MD5"));
        byte[] a = md.digest(b.getBytes());
        a = md.digest(a);
        a = md.digest(a);
        return bytesToHex(a);
    }
    
    public static void main(String[] args) {
    	System.out.println(encrypt("281841", 16).toLowerCase());
	}
}
