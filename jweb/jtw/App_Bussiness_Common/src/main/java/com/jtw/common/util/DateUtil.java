package com.jtw.common.util;

import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class DateUtil {
	
	private DateUtil() {}
	
	public static final String YM = "yyyy-MM";
	public static final String YMD_DATA = "yyyy-MM-dd";
	public static final String YMD01_DATA = "yyyy/MM/dd";
	public static final String YMDHMS_DATA = "yyyy-MM-dd HH:mm:ss";
	public static final String YMDSTRING_DATA = "yyyyMMddHHmmss";
	public static final String YMDHMSS = "yyyyMMddHHmmssSSS";
	public static final String YMDGB_DATA = "yyyy年MM月dd日";
	public static final String YMDTHMSGB_DATA = "yyyy年MM月dd日 HH时mm分ss秒";
	public static final String YMD24H_DATA = "yyyy-MM-dd HH:mm:ss";
	public static final String YMD = "yyyyMMdd";
	public static final String[] DAYNAMES = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };

	/**
	 * 一天的毫秒数
	 */
	public static final int ONE_DATE_MILLISECOND = 24 * 60 * 60 * 1000;

	/**
	 * 当前年
	 * @return
	 */
	public static int getYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	/**
	 * 当前月
	 * @return
	 */
	public static int getMonth() {
		return Calendar.getInstance().get(Calendar.MONTH)+1;
	}

	/**
	 * 当前日
	 * @return
	 */
	public static int getDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 当前周
	 * @return
	 */
	public static int getWeek() {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
        return c.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 当前星期
	 * @return
	 */
	public static String getDayOfWeek() {
		Calendar calendar = Calendar.getInstance();
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return DAYNAMES[dayOfWeek - 1];
	}

	/**
	 * 当前年月日
	 * @return
	 */
	public static String getYMD_CN() {
		Calendar now = Calendar.getInstance();
		return now.get(Calendar.YEAR) + "年" + (now.get(Calendar.MONTH) + 1) + "月" + now.get(Calendar.DAY_OF_MONTH) + "日";
	}

	/**
	 * Date转字符串
	 * @param date
	 * @param format
	 * @return
	 */
	public static String format(Date date, String format) {
		date = date == null ? new Date() : date;
		String result = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			result = sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 字符串转Date
	 * @param dateStr
	 * @param format
	 * @return
	 */
	public static Date parseDate(String dateStr, String format) {
		if (dateStr == null || dateStr.length() == 0) {
			return null;
		}
		Date date = new Date();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			date = null;
		}
		return date;
	}
	
	/**
	 * String转long
	 * @param date
	 * @param format
	 * @return
	 */
	public static long string2long(String date, String format) {
		try {
			if (StringUtils.isEmpty(format)) {
				format = YMDHMS_DATA;
			}
			SimpleDateFormat sf = new SimpleDateFormat(format);
			return sf.parse(date).getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0l;
	}
	
	/**
	 * long转string
	 * @param time
	 * @param format
	 * @return
	 */
	public static String long2String(long time, String format) {
		try {
			if (StringUtils.isEmpty(format)) {
				format = YMDHMS_DATA;
			}
			SimpleDateFormat sf = new SimpleDateFormat(format);
			Date date = new Date(time);
			return sf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 两日期相差X天
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDayDiff(Date start, Date end) {
		return (int) ((end.getTime() - start.getTime()) / ONE_DATE_MILLISECOND);
	}

	/**
	 * 指定日期加X天
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addDay(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + days);
		return calendar.getTime();
	}

	/**
	 * 指定日期加X月
	 * @param date
	 * @param months
	 * @return
	 */
	public static Date addMonth(Date date, int months) {
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.add(Calendar.MONTH, months);
		return calendar.getTime();
	}
	
	/**
	 * 指定日期加X毫秒数
	 * @param date
	 * @param millisecond
	 * @return
	 */
	public static String addTime(Date date, long millisecond) {
		SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str = "";
        try {
        	//1分钟60秒，1秒1000毫秒
            str = sim.format(date.getTime()+millisecond);
        } catch (Exception e) {
        	e.printStackTrace();
        }
		return str;
	}

	/**
	 * 当天结束时间
	 * @param date
	 * @return
	 */
	public static Date dayEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	/**
	 * 本周所有日期
	 * @return
	 */
	public static List<String> getWeekList() {
		List<String> list = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK );
        if (c.getFirstDayOfWeek() == Calendar.SUNDAY) {
            c.add(Calendar.DAY_OF_MONTH, 1);
        }
        c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
        for (int i=1;i<=7;i++) {
            c.add(Calendar.DAY_OF_MONTH, 1);
            list.add(sdf.format(c.getTime()));
        }
        return list;
	}
	
	/**
	 * 本月最后一天
	 * @return
	 */
	public static Date getLastDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	
	public static void main(String[] args) {
		System.out.println("当前年 - "+getYear());
		System.out.println("当前月 - "+getMonth());
		System.out.println("当前日 - "+getDay());
		System.out.println("当前周 - "+getWeek());
		System.out.println("当前星期 - "+getDayOfWeek());
		System.out.println("当前年月日 - "+getYMD_CN());
		System.out.println("Date转字符串 - "+format(new Date(), "yyyy-MM-dd HH:mm:ss"));
		System.out.println("字符串转Date - "+parseDate("2016-03-24 12:12:12", "yyyy-MM-dd HH:mm:ss"));
		System.out.println("String转long - "+string2long("2016-08-25","yyyy-MM-dd"));
		System.out.println("long转String - "+long2String(1472054400000L,""));
		System.out.println("两日期相差X天 - "+getDayDiff(new Date(), new Date()));
		System.out.println("指定日期加X天 - "+addDay(new Date(), 2));
		System.out.println("指定日期加X月 - "+addMonth(new Date(), 2));
		System.out.println("指定日期加X毫秒数 - "+addTime(new Date(), 20L * 60L * 1000L));
		System.out.println("当天结束时间 - "+dayEnd(new Date()));
		System.out.println("本周所有日期 - "+getWeekList());
		System.out.println("本月最后一天 - "+getLastDayOfMonth());
		
		System.out.println("Date转字符串 - "+format(new Date(), YM));
		
		System.out.println("Date转字符串 - "+format(new Date(), YMD_DATA));
	}
}
