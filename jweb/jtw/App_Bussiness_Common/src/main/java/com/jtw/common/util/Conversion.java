package com.jtw.common.util;

import java.math.BigDecimal;

public class Conversion {

	/**
	 * 四舍五入
	 * @param d
	 * @param length
	 * @return
	 */
	public static BigDecimal rounding(double d, int length) {
		BigDecimal digDecimal = new BigDecimal(d);
		return digDecimal.setScale(length, BigDecimal.ROUND_HALF_UP);
	}
}
