package com.jtw.common.token;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jtw.common.bean.vo.PagingVo;
import lombok.Data;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

/**
 * DESCRIPT:系统token类
 *
 * @author cjsky666
 * @date 2018/9/19 10:55
 */
@Data
public class ClientToken {
    static ObjectMapper objectMapper = new ObjectMapper();
    private Date exp = new Date(Calendar.getInstance().getTimeInMillis()+3600000);//默认1小时之后客户端本地判断登陆过期
    private Object info;
    private String crypt = "Base64";
    private Date iat = new Date();

    public static String createClientToken(ClientToken clientToken){
        try {
            return Base64.getEncoder().encodeToString(objectMapper.writeValueAsString(clientToken).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        ClientToken customToken = new ClientToken();
        customToken.setIat(new Date());
        customToken.setInfo(new PagingVo());
        String s = null;
        try {
            s = objectMapper.writeValueAsString(customToken);
            String a = Base64.getEncoder().encodeToString(s.getBytes("UTF-8"));
            String b = new String(Base64.getDecoder().decode(a),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
