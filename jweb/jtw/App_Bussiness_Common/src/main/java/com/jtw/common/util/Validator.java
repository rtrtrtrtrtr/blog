package com.jtw.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

	/**
     * 正则表达式：验证用户名
     */
    public static final String REGEX_USERNAME = "^[a-zA-Z]\\w{5,17}$";
 
    /**
     * 正则表达式：验证密码
     */
    public static final String REGEX_PASSWORD = "^[a-zA-Z0-9]{6,16}$";
 
    /**
     * 正则表达式：验证手机号
     */
    public static final String REGEX_MOBILE = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
 
    /**
     * 正则表达式：验证邮箱
     * /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/
     */
    public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
 
    /**
     * 正则表达式：验证汉字
     */
    public static final String REGEX_CHINESE = "^[\u4e00-\u9fa5],{0,}$";
 
    /**
     * 正则表达式：验证身份证
     */
    public static final String REGEX_ID_CARD = "(^\\d{18}$)|(^\\d{15}$)";
 
    /**
     * 正则表达式：验证URL
     */
    public static final String REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";
 
    /**
     * 正则表达式：验证IP地址
     */
    public static final String REGEX_IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
    /**
     * 正则表达式：验证是否是英文开头加数字
     */
    public static final String REGEX_EN_AND_NUMER = "^[A-Za-z]+\\d{1,19}$";

    /**
     * 正则表达式：过滤所有符号
     */
    public static final String REGEX_STRING_MARK="[\n`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";


    /**
     * 校验用户名
     * 
     * @param username
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUsername(String username) {
        return Pattern.matches(REGEX_USERNAME, username);
    }
 
    /**
     * 校验密码
     * 
     * @param password
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isPassword(String password) {
        return Pattern.matches(REGEX_PASSWORD, password);
    }
 
    /**
     * 校验手机号
     * 
     * @param mobile
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isMobile(String mobile) {
        return Pattern.matches(REGEX_MOBILE, mobile);
    }
 
    /**
     * 校验邮箱
     * 
     * @param email
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isEmail(String email) {
        return Pattern.matches(REGEX_EMAIL, email);
    }
 
    /**
     * 校验汉字 是否全部是汉字
     * 
     * @param chinese
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isChinese(String chinese) {
        return Pattern.matches(REGEX_CHINESE, chinese);
    }

    public static boolean isAllChinese(String chinese){
        int n = 0;
        for(int i = 0; i < chinese.length(); i++) {
            n = (int)chinese.charAt(i);
            if(!(19968 <= n && n <40869)) {
                return false;
            }
        }
        return true;
    }
 
    /**
     * 校验身份证
     * 
     * @param idCard
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isIDCard(String idCard) {
        return Pattern.matches(REGEX_ID_CARD, idCard);
    }
 
    /**
     * 校验URL
     * 
     * @param url
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUrl(String url) {
        return Pattern.matches(REGEX_URL, url);
    }
 
    /**
     * 校验IP地址
     * 
     * @param ipAddr
     * @return
     */
    public static boolean isIPAddr(String ipAddr) {
        return Pattern.matches(REGEX_IP_ADDR, ipAddr);
    }

    /**
     *
     * @param countname
     * @return
     */
    public static boolean isChineseInString(String str)
    {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param countname
     * @return
     */
    public static boolean isEnAndNumber(String str)
    {
        return Pattern.matches(REGEX_EN_AND_NUMER, str);
    }

    /**
     * 判断是否为奇数
     * @param args
     */
    public static boolean isOddNumber(int i){
        return  ((i & 1) == 1) ? true : false;
    }

    /**
     * 将字符串中的符号过滤
     * @param str
     * @return
     */
    public static String filterStringMark(String str){
        String aa = "";
        Pattern p = Pattern.compile(REGEX_STRING_MARK);
        Matcher m = p.matcher(str);
        return m.replaceAll(aa).trim();
    }

    /**
     * 将字符串中的符号过滤
     * @param str
     * @return
     */
    public static Boolean isStringMark(String str){
        String aa = "";
        Pattern p = Pattern.compile(REGEX_STRING_MARK);
        Matcher m = p.matcher(str);
        return m.matches();
    }


    /**
     * 将字符串转换成utf-8编码
     * @param args
     */


//可以在中括号内加上任何想要替换的字符


    public static void main(String[] args) {
//        String username = "kk8852d";
//        System.out.println(Validator.isChineseInString(username));
//        System.out.println(Validator.isAllChinese(username));
//        System.out.println(Validator.isEnAndNumber(username));
//
        System.out.println(isOddNumber(0));
    }
}
