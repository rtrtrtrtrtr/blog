package com.jtw.common.bean.Result;

import com.jtw.common.bean.enums.ReqCode;
import lombok.Data;

import java.util.Calendar;

/**
 * Created by lc on 2018/6/16.
 */
@Data
public class Message<T> {

    private Integer code;

    private String message;

    private Long  serviceTime;

    private T data;

    public Message() {
        this.serviceTime = Calendar.getInstance().getTimeInMillis();
    }

    public Message(Integer code, String message) {
        this.code = code;
        this.message = message;
        this.serviceTime = Calendar.getInstance().getTimeInMillis();
    }

    public Message(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.serviceTime = Calendar.getInstance().getTimeInMillis();
    }

    public static Message getSuccess() {
        return ReqCode.Success.getMessage();
    }

    public static Message getFailure() {
        return ReqCode.UnKnowException.getMessage();
    }
}
