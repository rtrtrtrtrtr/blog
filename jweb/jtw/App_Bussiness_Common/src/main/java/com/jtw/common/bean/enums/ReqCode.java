package com.jtw.common.bean.enums;

import com.github.pagehelper.Page;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.exception.BaseException;
import lombok.Data;

/**
 * 响应代码
 * Created by cjsky666 on 2018/7/17.
 */
public enum ReqCode {

    // code < 20000, 公共、全局响应
    Success(0, "SUCCESS"),
    AddSuccess(0,"新增成功"),
    UpdateSuccess(0,"修改成功"),
    DelSuccess(0,"删除成功"),
    SelectSuccess(0,"查询成功"),
    UploadSuccess(0,"上传成功"),
    SignUpSuccess(0,"注册成功"),
    OperateSuccess(0,"操作成功"),
    SubmitSuccess(0,"提交成功"),
    CancelSuccess(0,"取消成功"),
    ExchengeSuccess(0,"兑换成功"),
    CheckInSuccess(0,"签到成功"),

    Error404(404,"请求服务不存在"),
    Error401(401,"不见了"),
    Error402(402,"不见了"),
    Error403(403,"不见了"),
    Error500(500,"服务故障"),

    //1000-1099 登陆状态相关参数错误 需要重新登陆
    UnLogin(1000, "登录状态丢失"),
    CRSFTokenInvalid(1001, "令牌无效"),
    CRSFTokenExpired(1002, "令牌失效"),

    //2000-2099 需要提示具体信息 而不是message
    DuplicateKeyException(2000,"相似数据已存在"),
    OutOfRangeException(2001,"存储的数据过长"),
    FindParamsVoColumnError(2002,"筛选字段非法"),
    FindParamsVoConditionError(2003,"筛选规则非法"),
    FindParamsVoValueError(2004,"筛选输入值非法"),
    FindParamsVoPrefixError(2005,"筛选字段前缀非法"),
    FindParamsVoNoExitError(2006,"非法查询条件"),
    NoSupportChangeError(2007,"系统已禁止本次操作"),
    InActiveError(2008,"未激活"),
    DataLockedUpdateError(10102, "数据已变更,请重试"),

//  游戏业务返回码
    LotteryBetSuccess(0,"竞猜成功"),
    LotteryGameNoExist(3000,"游戏非法"),
    LotteryGamePlayNoExist(3001,"玩法类型非法"),
    LotteryGamePlayRuleNoExist(3002,"玩法规则非法"),
    LotteryGameLocked(3003,"游戏已关闭"),
    LotteryGamePlayLocked(3004,"玩法类型已关闭"),
    LotteryGamePlayRuleLocked(3005,"玩法规则已关闭"),
    LotteryOddsTypeConfigExecption(3006,"系统返点配置错误"),
    LotteryGameTimeNotOpen(3007,"投注时间未到"),
    LotteryBetBuyTimeClosed(3008,"本期已封盘"),
    LotteryBetBuyExecptIllegal(3009,"投注期号无效"),
    LotteryMemberBalanceNotEnough(3010,"余额不足"),
    LotteryMemberCoinNotEnough(3011,"代币不足"),
    LotteryMemberBetBuyAmountError(3012,"投注资金无效"),
    LotteryMemberBetTypeNotSupport(3013,"投注资金类型无效"),
    UnExistingBankException(3014,"该资金渠道不存在"),
    BankIsDisabledException(3015,"该资金渠道已禁用"),
    UnExistingMemaberBankException(3016,"提现账户不存在"),
    WithdrawChanelIsClosed(3017,"提现申请时间未到"),
    DataProcessERROR(3018,"当前数据流程错误"),
    OutOfMemberCardCountRangError(3019,"已达到最大限制"),
    NotBelongToUserError(3020,"资源不属于当前用户"),
    MemberOldPasswordIsInvalid(3020,"原始密码不正确"),
    MemberWithdrawPwdUnExisting(3021,"未设置提现密码"),
    MemberWithdrawPwdUError(3022,"账户提现密码错误"),
    MemberPwdApplyIsExistIng(3023,"已存在申请,请等待处理"),
    MemberInviteCodeLocked(3024,"当前邀请码已禁用"),
    MemberInViteCodeUnExisting(3025,"当前邀请码无效"),
    MemberAgentLocked(3026,"当前代理已锁定"),
    MemberAgentUnExisting(3027,"当前代理不存在"),
    MemberAgentApplyBalanceLess(3028,"当前流水额度不足"),
    ApplyExisting(3029,"请勿重复提交申请"),
    MemberAccountIsAgent(3030,"当前账号已经是代理了"),
    ExistWithAgentRewardStatistics(3031,"代理团队奖励已统计"),
    MemberAgentStateInvalid(3032,"当前代理已状态不可用"),
    MemberAgentLevelNoSupport(3033,"代理等级不够"),
    LotteryBetDataisEmpty(3034,"投注数据非法"),
    ExistWithAgentRecommendRewardStatistics(3035,"代理直推奖励已统计"),

    NotBelongForUserBetData(3036,"非法撤单数据"),
    LotteryExpectIsOpened(3037,"已开奖,无法撤单"),
    NotSupportCancelBet(3038,"已封盘,无法撤单"),
    NotSupportCancelBetTime(3039,"时间已过,无法撤单"),
    LotteryMemberBetChanelClosed(3040,"投注失败,通道已关闭"),
    LotteryMemberBetMoreThenCount(3041,"投注非法,号码数超出限制"),
    PlatfromFeatureDisabled(3042,"系统功能被禁用"),
    ExchangeAmountInvalid(3043,"兑换金额过低"),
    NotingToModify(3044,"修改无变化,请重试"),
    LotteryGameTypeNoExist(3045,"当前类型不存在"),
    MemerRegisterInviteCodeNotExiting(3046,"当前注册方式为邀请制"),
    ExistWithAgentDayRewardStatistics(3047,"代理流水奖励已统计"),
    MemberModifyIsNotAgent(3048,"当前被调整的账号不是代理"),
    MemeberCheckInCompelete(3049,"签到已完成"),
    MemeberCheckInChannelClosed(3050,"签到功能已关闭" ),
    MemeberCheckInTimeOutRange(3051,"签到时间未到" ),
    MemberCheckInFail(3052,"签到失败,请重试" ),
    MemberBalanceNotEnough(3053,"领取失败，账号资金不足"),
    OutOfWithdrawMaxCount(3054,"今日提现次数已满"),
    OutOfWithdrawMaxBalance(3055,"超出单次提现金额限制"),


    //抽奖提示码
    NotExistOfDraw(3056,"抽奖活动不存在"),
    UnCheckInToday(3057,"今日未签到"),
    NotEnoughBalance(3058,"积分不足"),
    DrawBalanceError(3059,"积分不足,请重新尝试"),
    DraDataError(3060,"抽奖失败,请重新尝试"),
    //赛事竞猜码
    UnExistingVersusError(3061,"对阵信息不存在"),
    OutOfVersusBetTimeRange(3062,"竞猜时间已过"),
    OutOfversusTeamIdRange(3063,"竞猜数据无效"),
    OutOfMaxBetCountError(3064,"您已经竞猜过本场比赛"),

    //直接提示统一信息 10000-19999
    UnKnowException(10000, "未知异常"),
    NoPermission(10001, "当前账号权限不足"),
    MobileFormatError(10002, "手机号码格式错误"),
    UserNameExisting(10003, "用户名已存在"),
    ForbidDeleteAdminRole(10004, "禁止删除管理员角色"),
    IndentifyCodeError(10005,"验证码错误"),
    NotSupportMethod(10006,"请求接口类型错误"),
    IndentifyCodeMissing(10007,"验证码已过期"),
    UploadFileNoExist(10008,"上传文件不存在"),
    UploadFileTypeError(10009,"上传文件类型错误"),
    UploadError(10010,"上传错误"),
    UploadFileNumLargerError(10011,"上传文件数量过多"),
    TaskPasueException(10012,"定时任务暂停失败"),
    TaskResumeException(10013,"定时任务恢复失败"),
    UnExistingDataException(10014,"操作的数据不存在"),
    isNotAdmin(10015,"请确认账号登陆权限"),
    isUserNameRangChinese(10016,"用户名不允许包含汉字"),
    HttpMethodNotSupport(10017,"请求方式不支持"),
    ForbidDeleteMemberRole(10018, "禁止删除会员角色"),
    isNotMobile(10019,"手机号码格式不正确"),
    ParamsValidError(10020, "参数校验错误"),
    GateWayError(10100, "网关服务已停止"),
    ServiceError(10101, "网关路由服务已停止"),
    isNotTrueAccountFormat(10102,"账号格式不正确"),


    //20000 - 40000 sys
    SysLoginPasswordError(20001, "登录密码错误"),
    SysLoginUnKnowAccount(20002, "登录账号不存在"),
    SysLoginExcessiveAttempts(20003, "登录过于频繁"),
    SysLoginLockedAccount(20004, "账号已锁定"),
    UnExistingSysRole(20005, "系统角色不存在"),
    UnExistingSysUserRole(20006, "系统用户角色不存在"),
    UnExistingSysRequest(20007, "系统请求不存在"),
    UnExistingSysMenuType(20008, "系统菜单类型不存在"),
    UnExistingSysMenuParent(20009, "系统菜单的父节点不存在"),
    UnExistingSysMenu(20010, "系统菜单不存在。"),
    SetSysRoleRequestFailed(20011, "设置系统角色请求权限失败"),
    UnExistingSysUser(20012, "系统用户不存在"),
    InvalidSysUserState(20013, "无效的系统用户状态"),
    SysUserNameisExist(20014,"账号已存在"),


    //40000 - 60000 org
    OrgLoginPasswordError(40001, "登录密码错误"),
    OrgLoginUnKnowAccount(40004, "登录账号不存在"),
    OrgLoginExcessiveAttempts(40003, "登录过于频繁"),
    OrgLoginLockedAccount(40004, "账号已锁定"),
    UnExistingOrgRole(40005, "机构角色不存在"),
    UnExistingOrgUserRole(40006, "机构用户角色不存在"),
    UnExistingOrgRequest(40007, "机构请求不存在"),
    UnExistingOrgMenuType(40008, "机构菜单类型不存在"),
    UnExistingOrgMenuParent(40009, "机构菜单的父节点不存在"),
    UnExistingOrgMenu(40010, "机构菜单不存在"),
    SetOrgRoleRequestFailed(40011, "设置机构角色请求权限失败"),
    UnExistingOrgUser(40012, "机构用户不存在"),
    InvalidOrgUserState(40013, "无效的机构用户状态"),
    ;

    //60000 - 80000 city


    //80000 - 100000 C端




    private Integer code;
    private String message;
    ReqCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


    public BaseException getException() {
        return getException(null);
    }

    public BaseException getException(Object data) {
        return new BaseException(this.code, this.message, data);
    }

    public Message getMessage() {
        return getMessage(null);
    }

    public Message getMessage(Object data) {
        if (data instanceof Page) {
            Page page = (Page) data;
            return new Messages(this.code, this.message, page, page.getTotal(), page.getPages());
        }else{
            return new Message(this.code, this.message, data);
        }
    }

    @Data
    class Messages extends Message {
        long total;
        int pages;

        public Messages(Integer code, String message, Object data, long total, int pages) {
            super(code, message, data);
            this.total = total;
            this.pages = pages;
        }
    }
}
