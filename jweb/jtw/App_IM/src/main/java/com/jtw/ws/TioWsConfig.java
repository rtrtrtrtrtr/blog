package com.jtw.ws;

import org.tio.utils.time.Time;

/**
 * 配置文件
 */
public abstract class TioWsConfig {
    /**
     * 协议名字(可以随便取，主要用于开发人员辨识)
     */
    public static final String PROTOCOL_NAME = "showcase";

    public static final String CHARSET = "UTF-8";

    /**
     * 监听端口
     */
    public static final int SERVER_PORT = 9326;

    /**
     * 心跳超时时间，单位：毫秒
     */
    public static final int HEARTBEAT_TIMEOUT = 1000 * 60;
    /**
     * 与客户端一致的指令长度协议
     */
    public static final int CMD_LEN= 5;
    /**
     * ip数据监控统计，时间段
     * @author tanyaowu
     */
    public  interface IpStatDuration {
        public static final Long DURATION_1 = Time.MINUTE_1 * 5;
        public static final Long[] IPSTAT_DURATIONS = new Long[]{DURATION_1};
    }

}
