package com.jtw.tcp;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/4/5 21:13
 */
public class Const {
    /**
     * 服务器地址
     */
    public static final String SERVER = "127.0.0.1";

    /**
     * 监听端口
     */
    public static final int PORT = 6789;

    /**
     * 心跳超时时间
     */
    public static final int TIMEOUT = 10000;

}
