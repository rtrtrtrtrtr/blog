package com.jtw.tcp;

import lombok.Data;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.Opcode;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/4/5 21:10
 */
public class TCPPacket extends Packet {
    private static final long serialVersionUID = -7957543938741369138L;
    public static final int		HEADER_LENGHT	= 4;
    public static final String CHARSET = "UTF-8";
    private byte[] body;

    /**
     * @return the body
     */
    public byte[] getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(byte[] body) {
        this.body = body;
    }

}
