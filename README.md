# blog

#### 介绍
web项目前后端分离部署的基础架子。包含管理端console、移动端mobile、电脑端pc和后台java代码。适用于个人、企业网站、论坛、OA、商城项目等等，配置好之后只需要编写新的业务逻辑就好。

本项目包含 登录、注册、权限管理、前后端主体框架页面等等基本的业务和功能。对于vue、java、element和vant等学习有借鉴和参考作用



#### 软件技术栈
1、后端java
    springboot+mybatis+shiro+Quartz
2、前端vue全家桶+element（PC端）、vue全家桶+vant（移动）

#### 安装部署教程

1. 前端项目分为 
    console（管理端）
    mobile（移动端）
    pc（电脑端）
    需要安装node环境8.0+（自带npm）。
    拉取代码之后，根目录 执行 npm install 初始化，安装对应的依赖库
    完成之后，执行npm run dev 启动，控制台会显示相应的访问地址
    可以在config中  修改后端ip地址 或者使用localhost
2. 后端项目需要安装maven
    使用idea或者ecplise打开之后，默认会加载所需要的依赖模块。
    业务代码在 app_user模块下（有需要自行修改） ，在此模块的resource中
    配置好数据库链接地址。则可以启动后台系统
    
3. jweb下的dev_common为一些依赖库 shiro redis mybatis datasources等等项目的基础配置。单应用系统默认不需要更改。若有更改，修改完毕之后在 dev_common下执行mvn install后 即可 被jtw模块下的应用自动依赖，这是在maven中已配置好的
4. jtw下的模块为业务模块及一个业务所依赖的工具模块。为了方便 App_Bussiness_Common模块就直接放到这边了，
    否则，如果有新的工具类或者方案增加，每次都需要在dev_common下 执行打包，太麻烦了
5. 移动端可以封装为webapp


#### 项目介绍（请看项目截图）
   ![移动端](https://images.gitee.com/uploads/images/2019/0905/100809_881db1e4_665055.png "微信图_201909051007317.png")
![移动端](https://images.gitee.com/uploads/images/2019/0905/103917_87869776_665055.png "微信图片_201909051007313.png")
![移动端](https://images.gitee.com/uploads/images/2019/0905/103954_be205a4e_665055.png "微信图片_201909051007314.png")
![移动端](https://images.gitee.com/uploads/images/2019/0905/104010_73763364_665055.png "微信图片_201909051007315.png")

#### PC端
![PC端](https://images.gitee.com/uploads/images/2019/0905/104432_0d21645c_665055.png "微信图片_2019090510073111.png")
![PC端](https://images.gitee.com/uploads/images/2019/0905/104443_14954bc0_665055.png "微信图片_2019090510073112.png")

#### 管理后台
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160347_82c7d3f1_665055.png "458e196ce0d831d9b382a04595f36ac.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160357_68b79c3a_665055.png "ef200fe4f6ddcc68b2e813155223cdf.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160407_8d26c828_665055.png "2681dcc3c996ff618ed463668601920.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160413_ad4f4c5b_665055.png "2aede701b143c9f47febe790daf6fa5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160421_23f84f83_665055.png "90f9eea1df7fc6762800567f427b9f9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160430_71e21d53_665055.png "60605ca86830d7477d6c7c7e47fe6c0.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160639_c01b781e_665055.png "8937bcbea2f2dc8257e5aa82817fc18.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160648_19a25df7_665055.png "da70ce22672c2baa96838a0677adbe0.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/160739_be58cac8_665055.png "22a6a13356200d3d5e0b98917f179d3.png")
#### 感谢开源社区

#### 许可证
   [MIT](LICENSE "MIT")
    
