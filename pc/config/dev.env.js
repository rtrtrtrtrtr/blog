'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://192.168.2.109:8006/"',
  // RESOURCE_DOMAIN:'"http://192.168.2.109:8006"',
  // WS_DOMAIN:'"192.168.2.109"',
  BASE_API: '"http://localhost:8006/"',
  RESOURCE_DOMAIN:'"http://localhost:8006"',
  WS_DOMAIN:'"localhost"',
  PROJECT_NAME:'"SKY_CLIENT_PC"',
  EXP:3600000
})
