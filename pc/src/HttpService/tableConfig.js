// 客户端筛选功能文件
// conditions
// state=0表示该字段没有设置筛选为state=1标志该字段设置了条件
// type=1 输入框类型 type=2 时间类型 type=3 下拉框类型 筛选对比条件 默认为conditions 下的options中的第一个
// 该字段是否需要排序。若需要排序orderby=0 {降序,升序}
import memberBetLog from "./table/memberBetLog"
import memberAssetLog from "./table/memberAssetLog"
import memberRechargeLog from "./table/memberRechargeLog"
import memberWithdrawLog from "./table/memberWithdrawLog"
import teamMember from "./table/teamMember"
import teamMemberBetLog from "./table/teamMemberBetLog"
import teamMemberAssetLog from "./table/teamMemberAssetLog"
import profitAndLossAssetLog from "./table/profitAndLossAssetLog"
import teamSubAgent from "./table/teamSubAgent"
import teamRewardLog from "./table/teamRewardLog"
import teamRewardLogDetail from "./table/teamRewardLogDetail"
import teamRewardAgentLog from "./table/teamRewardAgentLog"
import teamRewardAgentLogDetail from "./table/teamRewardAgentLogDetail"
import teamRewardAgentLogDetailInfo from "./table/teamRewardAgentLogDetailInfo"
import teamDayRewardLog from "./table/teamDayRewardLog"
import teamDayRewardLogDetail from "./table/teamDayRewardLogDetail"
import memberCheckIn from "./table/memberCheckIn"

var tableConfig = {
  memberBetLog,
  memberAssetLog,
  memberRechargeLog,
  memberWithdrawLog,
  teamMember,
  teamMemberBetLog,
  teamMemberAssetLog,
  profitAndLossAssetLog,
  teamSubAgent,
  teamRewardLog,
  teamRewardAgentLog,
  teamRewardLogDetail,
  teamRewardAgentLogDetail,
  teamRewardAgentLogDetailInfo,
  teamDayRewardLog,
  teamDayRewardLogDetail,
  memberCheckIn,
};
console.log(tableConfig);
export default tableConfig;
