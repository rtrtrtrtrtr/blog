export default {
  id:{
    params: {
      value: "",
      column: "id",
      condition: "=",
      prefix: "`t_sys_agent_reward_statistics_info_agent`.",
      isString:false,
    },
    show:false,
    orderBy: {
      state: 1,
      index: 0,
      value: '',
      options: []
    },
    alias: '等于',
    state: 0,
    label: "结算ID",
    type: 3,
    conditions: {
      options: [
      ]
    }
  },
  level:{
    params: {
      value: "",
      column: "level",
      condition: "=",
      prefix: "`t_sys_agent_reward_statistics_info_agent`.",
      isString:false,
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '等于',
    state: 0,
    label: "自身等级",
    type: 3,
    conditions: {
      options: [
        {opt:3,alias:'初级代理'},
        {opt:4,alias:'中级代理'},
        {opt:5,alias:'高级代理'},
      ]
    }
  }
}
