if (Number.parseInt === undefined) Number.parseInt = window.parseInt;
if (Number.parseFloat === undefined) Number.parseFloat = window.parseFloat;
// import Vue from 'vue' //外部引入的形式不需要这个地方使用了
import App from './App';
import router from './router/routerscfg';
import store from '@/vuex/store';
import api from '@/HttpService/api';
import util from '@/utils/pro_util';
import PRO_CONSTANT from '@/utils/PRO_CONSTANT';


Vue.config.productionTip = false;
Vue.prototype.proAPI = api;
Vue.prototype.proUtil = util;
Vue.prototype.PRO_CONSTANT = PRO_CONSTANT;

new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  render: h => h(App)
})
