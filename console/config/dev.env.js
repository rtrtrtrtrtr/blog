'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://192.168.2.143:8006/"',
  // RESOURCE_DOMAIN:'"http://192.168.2.143:8006"',
  // WS_DOMAIN:'"192.168.2.143"',
  BASE_API: '"http://localhost:8006/"',
  RESOURCE_DOMAIN:'"http://localhost:8006"',
  WS_DOMAIN:'"localhost"',
  PROJECT_NAME:'"SKY_SERVER"',
  EXP:3600000
})
