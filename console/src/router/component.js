const Container = () => import ('@/components/Container')
const Main = () => import ('@/components/Main')
const hello00 = () => import ('@/components/HelloWorld00')
const v_login = () => import ('@/views/view_Login')
const v_sysMapping = () => import ('@/components/modules/sysManagement/perm/view_list_sysMapping')
const v_sysMenus = () => import ('@/components/modules/sysManagement/menu/view_list_sysMenu')
const v_sysRoles = () => import ('@/components/modules/sysManagement/role/view_list_sysRoles')
const v_sysUsers = () => import ('@/components/modules/sysManagement/user/view_list_sysUsers')
const v_sysMembers = () => import ('@/components/modules/sysManagement/user/view_list_sysMembers')
const v_sysTask = () => import ('@/components/modules/sysManagement/task/view_list_sysTask')
const v_sysBank = () => import ('@/components/modules/sysManagement/bank/view_list_sysBank')
const v_sysConfig = () => import("@/components/modules/sysManagement/system/view_list_sysConfig")
const v_memberAssetChangeLog = () => import("@/components/modules/sysManagement/member/view_list_memberAssetChangeLog")
const v_memberAsset = () => import("@/components/modules/sysManagement/member/view_list_memberAsset")
const v_clientBgImg = () => import("@/components/modules/sysManagement/system/view_list_bgImg")
const v_webLocgo = () => import("@/components/modules/sysManagement/system/view_list_webLogo")
const v_memberPwdReset = () => import("@/components/modules/sysManagement/member/view_list_memberPwdReset")
const v_sysBanner = () => import("@/components/modules/sysManagement/banner/view_list_banner.vue")
const v_sysInformation = () => import("@/components/modules/sysManagement/information/view_list_information.vue")
const v_accessLog = () => import("@/components/modules/sysManagement/system/view_list_accessLog")
const v_sysLogFile = () => import("@/components/modules/sysManagement/system/view_list_systemLogFile")
const v_MemberCheckIn= () => import ('@/components/modules/sysManagement/member/view_list_checkInLog')


/**
 * URI资源对应的路由组件,
 * @type {{path: string, name: string, component: (function(): (Promise<*>|*)), children: *[]}}
 */
const  AuthorizationURI = {
  path: '/home',
  name: '后台管理中心',
  component: Container,
  children: [
    {
      name: "权限管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/perms/findAll', component: v_sysMapping, name: "系统权限接口列表"},
        {path: '/sys/menus/findAll', component: v_sysMenus, name: "系统菜单列表"}
      ]
    },
    {
      name: "角色管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/roles/findAll', component: v_sysRoles, name: "系统角色列表"}
      ]
    },
    {
      name: "配置管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/system/findAll', component: v_sysConfig, name: "系统配置列表"}
      ]
    },
    {
      name: "用户管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/users/findAll', component: v_sysUsers, name: "平台管理员列表"},
        {path: '/sys/members/findAll', component: v_sysMembers, name: "系统会员列表"}
      ]
    },
    {
      name: "任务管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/task/findAll', component: v_sysTask, name: "系统任务列表"}
      ]
    },
    {
      name: "收费管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/bank/findAll', component: v_sysBank, name: "收款账号"}
      ]
    },
    {
      name: "会员功能",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/member/pwdResetApply', component: v_memberPwdReset, name: "登陆密码重置申请"},
        {path: "/sys/member/checkInLog",component: v_MemberCheckIn,  name: "签到记录"},
      ]
    },
    {
      name: "客户端设置",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/client/bgImg', component: v_clientBgImg, name: "背景图设置"},
        {path: '/sys/client/banner', component: v_sysBanner, name: "轮播图管理"},
        {path: '/sys/client/webLogo', component: v_webLocgo, name: "网站LOGO管理"}
      ]
    },
    {
      name: "资讯公告",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/information/findAll', component: v_sysInformation, name: "资讯列表"},
      ]
    },
    {
      name: "日志记录",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/log/findAllAccessLog', component: v_accessLog, name: "用户访问记录"},
        {path: '/sys/log/getAllSysLogFile', component: v_sysLogFile, name: "系统日志文件"},
      ]
    },
    {
      name: "资金管理",
      path: "/sys",
      component: Main,
      children: [
        {path: '/sys/member/asset', component: v_memberAsset, name: "会员积分"},
        {path: '/sys/member/AssetChangeLog', component: v_memberAssetChangeLog, name: "积分记录"},
      ]
    }
  ]
}

export default [
  {
    path: '/404',
    component: hello00,
    name: "互联网的荒原"
  },
  {
    path: '/',
    redirect: '/home'
  },
  AuthorizationURI
  ,
  {
    path: '/login',
    component: v_login,
    name: "登录"
  },
  {
    path: '*',
    redirect: "/404"
  }
]
