/**
 * 需要录入系统的菜单URI
 */
export default [
  {
    "id": 2,
    "name": "用户管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "2-1",
        "name": "管理员",
        "path": "/sys/users/findAll",
        "type": 2,
        children: []
      },
      {
        "id": "2-2",
        "name": "注册会员",
        "path": "/sys/members/findAll",
        "type": 2,
        children: []
      }
    ]
  },
  {
    "id": 3,
    "name": "系统管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "3-1",
        "name": "权限列表",
        "path": "/sys/perms/findAll",
        "type": 2,
        children: []
      },
      {
        "id": "3-2",
        "name": "菜单列表",
        "path": "/sys/menus/findAll",
        "type": 2,
        children: []
      },

      {
        "id": "3-3",
        "name": "角色列表",
        "path": "/sys/roles/findAll",
        "type": 2,
        children: []
      },

      {
        "id": "3-4",
        "name": "定时任务",
        "path": "/sys/task/findAll",
        "type": 2,
        "children": []
      },
      {
        "id": '3-5',
        "name": "初始化配置",
        "path": "/sys/system/findAll",
        "type": 2,
        "children": [
        ]
      }
    ]
  },
  {
    "id": 5,
    "name": "会员功能",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "5-1",
        "name": "登陆密码重置",
        "path": "/sys/member/pwdResetApply",
        "type": 2,
        "children": [
        ]
      },
      {
        "id": "5-3",
        "name": "签到记录",
        "path": "/sys/member/checkInLog",
        "type": 2,
        "children": [
        ]
      },
    ]
  },
  {
    "id": 6,
    "name": "客户端设置",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "6-1",
        "name": "背景图更新",
        "path": "/sys/client/bgImg",
        "type": 2,
        "children": [
        ]
      },
      {
        "id": "6-2",
        "name": "轮播图管理",
        "path": "/sys/client/banner",
        "type": 2,
        "children": [
        ]
      },{
        "id": "6-3",
        "name": "网站LOGO",
        "path": "/sys/client/webLogo",
        "type": 2,
        "children": [
        ]
      }
    ]
  },
  {
    "id": 7,
    "name": "资讯管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "7-1",
        "name": "资讯列表",
        "path": "/sys/information/findAll",
        "type": 2,
        "children": [
        ]
      }
    ]
  },
  {
    "id": 8,
    "name": "积分管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": 8-1,
        "name": "会员积分",
        "path": "/sys/member/asset",
        "type": 2,
        "children": [
        ]
      },
      {
        "id": 8-2,
        "name": "积分记录",
        "path": "/sys/member/assetChangeLog",
        "type": 2,
        "children": [
        ]
      },
    ]
  },
  {
    "id": 9,
    "name": "日志管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "9-1",
        "name": "用户访问日志",
        "path": "/sys/log/findAllAccessLog",
        "type": 2,
        "children": [
        ]
      },
      {
        "id": "9-2",
        "name": "系统日志文件",
        "path": "/sys/log/getAllSysLogFile",
        "type": 2,
        "children": [
        ]
      }
    ]
  }
]
