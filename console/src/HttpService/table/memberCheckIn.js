export default {
  state: {
    params:{
      value:"",
      column:"state",
      condition:"=",
      prefix:"`t_sys_member_check_in_reward`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'',
    state:0,
    label:"状态",
    type:3,
    conditions:{
      options:[
        {opt:"0",alias:"待发放"},
        {opt:"1",alias:"已发放"},
        {opt:"2",alias:"已拒绝"}
      ]
    }
  },
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:"`t_sys_member_check_in`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime",
      prefix: "`t_sys_member_check_in`."
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "日期",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_check_in`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "起始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_check_in`.",
        },
        alias: '小于等于',
        state: 0,
        label: "截至时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
}
