export default {
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:"`t_sys_user`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  balance: {
    params:{
      value:"",
      column:"balance",
      condition:"=",
      prefix:"",
      isString:false
    },
    orderBy:{
      state:1,
      index:0,
      value: '',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    alias:'等于',
    state:0,
    label:"账户积分",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:">",alias:"大于"},
        {opt:"<",alias:"小于"},
        {opt:">=",alias:"大于等于"},
        {opt:"<=",alias:"小于等于"}
      ]
    }
  },
  freezeBalance: {
    params:{
      value:"",
      column:"freezeBalance",
      condition:"=",
      prefix:"",
      isString:false
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"冻结积分",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:">",alias:"大于"},
        {opt:"<",alias:"小于"},
        {opt:">=",alias:"大于等于"},
        {opt:"<=",alias:"小于等于"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "创建时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_asset`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_asset`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
};
