// 客户端筛选功能文件
// conditions
// state=0表示该字段没有设置筛选为state=1标志该字段设置了条件
// type=1 输入框类型 type=2 时间类型 type=3 下拉框类型 筛选对比条件 默认为conditions 下的options中的第一个
// 该字段是否需要排序。若需要排序orderby=0 {降序,升序}
import sysUsers from "./table/sysUsers"
import sysTasks from "./table/sysTasks"
import sysConfigs from "./table/sysConfigs"
import memberAssetLog from "./table/memberAssetLog"
import memberAsset from "./table/memberAsset"
import memberPwd from "./table/memberPwd"
import sysBanner from "./table/sysBanner"
import sysInformation from "./table/sysInformation"
import sysAccessLog from "./table/sysAccessLog"
import memberCheckIn from "./table/memberCheckIn"

var tableConfig = {
  sysUsers,
  sysTasks,
  sysConfigs,
  memberAssetLog,
  memberAsset,
  memberPwd,
  sysBanner,
  sysInformation,
  sysAccessLog,
  memberCheckIn,
};
export default tableConfig;
